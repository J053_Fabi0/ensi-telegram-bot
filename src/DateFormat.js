/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 * Returns a string with a the date format like YYYY-MM-DD. If no date is given, it picks today as the date.
 * @returns string
 */
function getOnlyDate(date = null, noYear = false, truncatedYear = false) {
  // Original code taken from Sourav: https://stackoverflow.com/questions/6002254/get-the-current-year-in-javascript#answer-6002265

  // Return today's date and time
  const currentTime = date ? date : new Date();
  // returns the month (from 0 to 11)
  const month = currentTime.getMonth() + 1;
  // returns the day of the month (from 1 to 31)
  const day = currentTime.getDate();
  // returns the year (four digits)
  const year = truncatedYear ? currentTime.getFullYear().toString().substring(2, 4) : currentTime.getFullYear();

  return `${noYear ? "" : year + "-"}${month <= 9 ? "0" + month : month}-${day <= 9 ? "0" + day : day}`;
}

function getDateFromString(dateStr) {
  return new Date(Date.parse(dateStr));
}

/**
 * Returns the actual year.
 * @returns integer
 */
function getActualYear() {
  return new Date().getFullYear();
}

/**
 * Returns the actual month.
 * @returns integer
 */
function getActualMonth() {
  return `${new Date().getMonth() + 1 <= 9 ? "0" : ""}${new Date().getMonth() + 1}`;
}

/**
 * Given an array with the year, month and day, it returns if it's a valid date or not. Date is not valid if it's in the future.
 * @returns bool
 */
function checkIfValidDateArr(dateArr) {
  // just in case the array is made out of strings. You never know
  dateArr = dateArr.map((value) => Number(value));

  const [year, month, day] = dateArr;

  // Check if the year isn't higher than the actual year
  if (year > new Date().getFullYear()) return false;
  // Then if the month is not greater than the actual month if the year is the same
  if (year == new Date().getFullYear() && month > new Date().getMonth() + 1) return false;
  // And if the day is not greater, if the month and year are the same
  if (year == new Date().getFullYear() && month == new Date().getMonth() + 1 && day > new Date().getDate()) return false;

  return day <= getMonthDays(year, month);
}

function getMonthDays(year, month) {
  // Check if the day of the month isn't higher than it can be
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31;

    case 2:
      return getYearDays(year) == 366 ? 29 : 28;

    case 4:
    case 6:
    case 9:
    case 11:
      return 30;

    default:
      return 30;
  }
}

function getYearDays(year) {
  return !(year % 4) && (year % 100 || !(year % 400)) ? 366 : 365;
}

/**
 * Given an array with dates in format YYYY-MM-DD, it returns the lower date in the same format.
 * @returns string
 */
function getOldestDate(dates) {
  // All dates are like 2020-10-18, so I delete all the - and make the result a number: 20201018
  dates = dates.map((value) => Number(value.replace(/-/g, "")));

  // Then just pick up the largest number in the array, convert it to string...
  const oldestDate = dates
    .reduce(function (a, b) {
      return Math.min(a, b);
    })
    .toString();

  // ...and return a bunch of substrings, joining again the year, month and day with "-"s
  return `${oldestDate.substring(0, 4)}-${oldestDate.substring(4, 6)}-${oldestDate.substring(6, 8)}`;
}

/**
 * Given an array with dates in format YYYY-MM-DD, it returns the greater date in the same format.
 * @returns string
 */
function getGreaterDate(dates) {
  // All dates are like 2020-10-18, so I delete all the - and make the result a number: 20201018
  dates = dates.map((value) => Number(value.replace(/-/g, "")));

  // Then just pick up the largest number in the array, convert it to string...
  const mostRecentDate = dates
    .reduce(function (a, b) {
      return Math.max(a, b);
    })
    .toString();

  // ...and return a bunch of substrings, joining again the year, month and day with "-"s
  return `${mostRecentDate.substring(0, 4)}-${mostRecentDate.substring(4, 6)}-${mostRecentDate.substring(6, 8)}`;
}

/**
 * Returns the days in between the two dates. Dates must be strings with format YYYY-MM-DD. It detects the greater one.
 * @returns integer
 */
function getDaysFromDateToDate(date1, date2) {
  const greaterDate = getGreaterDate([date1, date2]);

  // These will be equal to the greater date
  const [year1, month1, day1] = (greaterDate == date1 ? date1 : date2).split("-").map((value) => Number(value));

  // These will be equal to the lower date
  const [year2, month2, day2] = (greaterDate != date1 ? date1 : date2).split("-").map((value) => Number(value));

  let days = 0;
  for (let year = year2; year <= year1; year++) {
    if (year == year1)
      for (let month = year1 == year2 ? month2 : 1; month <= month1; month++) {
        if (month == month1) days += year1 == year2 && month1 == month2 ? day1 - day2 : day1;
        else if (month == month2) days += getMonthDays(year, month) - day2;
        else days += getMonthDays(year, month);
      }
    else if (year == year2)
      for (let month = month2; month <= 12; month++) {
        if (month == month2) days += getMonthDays(year, month) - day2;
        else days += getMonthDays(year, month);
      }
    else days += getYearDays(year);
  }

  return days;
}

/**
 * Returns a string with a date format like YYYY-MM-01 with the result of today date minus the months for the parameter m.
 */
function getDateMinusMonths(m = 1) {
  const date = getOnlyDate();
  const [year, month] = date.split("-").map((value) => Number(value));

  let newMonth = month - m;
  let newYear = year;

  if (newMonth <= 0) {
    newYear = year + Math.ceil(newMonth / 12) - 1;
    newMonth = 12 + (newMonth % 12);
  }

  return `${newYear}-${newMonth < 10 ? "0" : ""}${newMonth}-01`;
}

exports.getOnlyDate = getOnlyDate;
exports.getDateFromString = getDateFromString;
exports.getActualYear = getActualYear;
exports.getActualMonth = getActualMonth;
exports.checkIfValidDateArr = checkIfValidDateArr;
exports.getMonthDays = getMonthDays;
exports.getGreaterDate = getGreaterDate;
exports.getOldestDate = getOldestDate;
exports.getDaysFromDateToDate = getDaysFromDateToDate;
exports.getDateMinusMonths = getDateMinusMonths;
