/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const Extra = require("telegraf/extra");
const DateFormat = require("../DateFormat");

const { db, timeOut } = require("../../main");
const { userNotInDB } = require("../messages");
const { getPRVPrice } = require("../PRVPrice");
const { isUserInDB } = require("../isUserInDB");
const { handleErr } = require("../handleError");
const { specialEmoji: SE } = require("../messages");

const Nodes = require("../classes/Nodes");
const Accounts = require("../classes/Accounts");

let seeNode = null;
let seeAccount = null;
let prv_price = null;
let nodesNames = null;
let showInUSD = null;
let seeingOptions = false;

const nodesClass = new Nodes(db);
const accountsClass = new Accounts(db);

const a = {};

let timeOutF = null;
a.leave = () => clearTimeout(timeOutF);
a.used = (ctx, next) => {
  clearTimeout(timeOutF);
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  next(ctx);
};

a.showAccounts = async function (ctx, editMessage) {
  try {
    const numberOfAccounts = await accountsClass.getUserAccounts(ctx.from.id.toString());

    nodesClass.accountID = numberOfAccounts[0];
    nodesNames = await nodesClass.getNodesNames();
    await accountsClass.showAccounts(ctx, editMessage, false, true, null, false, true);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.showNode = async function (ctx, nodeName) {
  if (showInUSD) prv_price = await getPRVPrice();

  nodesClass.nodeName = nodeName || nodesClass.nodeName;
  nodesNames = nodesNames || (await nodesClass.getNodesNames());

  const nodeNameIndex = nodesNames.findIndex((value) => nodeName == value);

  const thisMonth = DateFormat.getOnlyDate();

  const earnings = await nodesClass.getTotalEarningsForNode();
  const average = await nodesClass.getAverageEarningsForNode();
  const times = await nodesClass.getHowManyTimesItEarned();

  const totalThisMonth = await nodesClass.getTotalEarningsForNode(thisMonth);
  const averageThisMonth = await nodesClass.getAverageEarningsForNode(thisMonth);
  const timesThisMonth = await nodesClass.getHowManyTimesItEarned(thisMonth);

  const oldestEarningDate = await nodesClass.getOldestEarningDateNode();
  const lastEarningDate = await nodesClass.getLastEarningDateNode();
  const daysSinceLastEarning = await nodesClass.getDaysSinceLastEarningNode(lastEarningDate);

  const currencyBtn = Markup.callbackButton(
    `${SE} Currency: ${showInUSD ? "USD" : "PRV"} ${SE}`,
    `changeCurrency_${nodeName}`
  );

  const [emoji, roleDescription] = await nodesClass.getEmojiOfNodeName(nodeName);

  // Set the buttons to access the contiguous nodes
  let contiguousNodes = [];
  if (nodesNames.length > 1) {
    if (nodeNameIndex == 0 && nodesNames[1]) {
      if (nodesNames.length > 2)
        contiguousNodes.push(
          Markup.callbackButton(
            `\u2935 ${nodesNames[nodesNames.length - 1]}`,
            `select_${nodesNames[nodesNames.length - 1]}`
          )
        );
      contiguousNodes.push(Markup.callbackButton(`${nodesNames[1]} \u2B07`, `select_${nodesNames[1]}`));
    } else if (nodeNameIndex == nodesNames.length - 1) {
      const condition = nodesNames.length > 2;
      contiguousNodes.push(
        Markup.callbackButton(
          `${condition ? "\u2B06 " : ""}${nodesNames[nodesNames.length - 2]}${condition ? "" : " \u2B06"}`,
          `select_${nodesNames[nodesNames.length - 2]}`
        )
      );
      if (condition)
        contiguousNodes.push(Markup.callbackButton(`${nodesNames[0]} \u2934`, `select_${nodesNames[0]}`));
    } else {
      contiguousNodes.push(
        Markup.callbackButton(`\u2B06 ${nodesNames[nodeNameIndex - 1]}`, `select_${nodesNames[nodeNameIndex - 1]}`)
      );
      contiguousNodes.push(
        Markup.callbackButton(`${nodesNames[nodeNameIndex + 1]} \u2B07`, `select_${nodesNames[nodeNameIndex + 1]}`)
      );
    }
  }

  // Prepare the text for last earning date
  let textForLastEarning = "\n\n<b>Last earning date:</b> ";
  if (lastEarningDate) {
    if (daysSinceLastEarning == 0) textForLastEarning = textForLastEarning + "today";
    else
      textForLastEarning =
        textForLastEarning +
        `<code>${lastEarningDate}</code> (${daysSinceLastEarning} day${daysSinceLastEarning == 1 ? "" : "s"})`;
  } else {
    textForLastEarning = textForLastEarning + "no registered earnings";
  }
  textForLastEarning = textForLastEarning + ".\n";

  // Prepare the text for oldest earning date
  let textForOldestEarning = "<b>First earning date:</b> ";
  if (oldestEarningDate) {
    if (oldestEarningDate === thisMonth) textForOldestEarning = textForOldestEarning + "today";
    else textForOldestEarning = textForOldestEarning + `<code>${oldestEarningDate}</code>`;
  } else {
    textForOldestEarning = textForOldestEarning + "no registered earnings";
  }
  textForOldestEarning = textForOldestEarning + ".";

  // Prepare the text for the n months in the past
  const { monthsInThePast } = await accountsClass.getSettings();
  let textForOtherMonths = "";
  for (let i = 1; i <= (monthsInThePast === undefined ? 2 : Number(monthsInThePast)); i++) {
    const lastMonth = DateFormat.getDateMinusMonths(i);

    const totalLastMonth = await nodesClass.getTotalEarningsForNode(lastMonth);
    const averageLastMonth = await nodesClass.getAverageEarningsForNode(lastMonth);
    const timesLastMonth = await nodesClass.getHowManyTimesItEarned(lastMonth);

    textForOtherMonths =
      textForOtherMonths +
      `\n\n<b>Earnings ${i} month${i > 1 ? "s" : ""} ago</b>` +
      "\n· <b>Total:</b> <code>" +
      (showInUSD ? Number((totalLastMonth * prv_price).toFixed(4)) : totalLastMonth) +
      `</code> ${showInUSD ? "USD" : "PRV"}.` +
      `\n· <b>Times it earned:</b> <code>${timesLastMonth}</code>.` +
      "\n· <b>Average earning:</b> <code>" +
      (showInUSD ? Number((averageLastMonth * prv_price).toFixed(4)) : averageLastMonth) +
      `</code> ${showInUSD ? "USD" : "PRV"}.`;
  }

  // Prepare the buttons for the "more options" case
  const nodePublicKey = await nodesClass.getPublicKeyOfNode();
  const optionsButtons = [];
  optionsButtons.push([
    Markup.callbackButton(`\u2B05 Go back ${SE}`, `seeLessOptions_${nodeName}`),
    Markup.callbackButton(`\u{1F5D1} Delete it ${SE}`, `askDelete_${nodeName}`),
  ]);
  if (!nodePublicKey)
    optionsButtons.push([
      Markup.callbackButton(`\u{1F511}\u{1F440} Add public key ${SE}`, `addPublicKey_${nodeName}`),
    ]);
  else
    optionsButtons.push([
      Markup.callbackButton(
        `\u{1F504}\u{1F511} Change public key ${SE}`,
        `changePublicKey_${nodesClass.accountID}_${nodeName}`
      ),
    ]);

  // Edit the message to show the final result
  await ctx.editMessageText(
    `<b><u>${nodesClass.nodeName}</u></b> ${SE}` +
      `\n${emoji} <i>${roleDescription}</i>.` +
      "\n\n<b>Earnings this month</b>" +
      "\n· <b>Total:</b> <code>" +
      (showInUSD ? Number((totalThisMonth * prv_price).toFixed(4)) : totalThisMonth) +
      `</code> ${showInUSD ? "USD" : "PRV"}.` +
      `\n· <b>Times it earned:</b> <code>${timesThisMonth}</code>.` +
      "\n· <b>Average earning:</b> <code>" +
      (showInUSD ? Number((averageThisMonth * prv_price).toFixed(4)) : averageThisMonth) +
      `</code> ${showInUSD ? "USD" : "PRV"}.` +
      textForOtherMonths +
      "\n\n<b>Earnings since the begining until now</b>" +
      `\n· <b>Total:</b> <code>` +
      `${showInUSD ? Number((earnings * prv_price).toFixed(4)) : earnings}</code> ${showInUSD ? "USD" : "PRV"}.` +
      `\n· <b>Times it earned:</b> <code>${times}</code>.` +
      `\n· <b>Average earning:</b> <code>${
        showInUSD ? Number((average * prv_price).toFixed(4)) : average
      }</code> ` +
      (showInUSD ? "USD." : "PRV.") +
      textForLastEarning +
      textForOldestEarning +
      (seeingOptions && nodePublicKey ? "\n\nPublic key: <code>" + nodePublicKey + "</code>." : "") +
      (showInUSD ? "\n\nThe PRV is at <code>" + prv_price + "</code> USD." : ""),

    Extra.markup(
      Markup.inlineKeyboard(
        seeingOptions
          ? optionsButtons
          : [
              [
                Markup.callbackButton(`\u2B05 Go back`, "back"),
                Markup.callbackButton(`\u2699 More options`, `seeMoreOptions_${nodeName}`),
              ],
              [currencyBtn],
              contiguousNodes,
              [Markup.callbackButton(`${SE || "\u{1F4B8}"} Add earning entry ${SE}`, "addEarning")],
            ]
      )
    ).HTML()
  );
};

a.enter = async function (ctx) {
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  try {
    const id = ctx.from.id.toString();
    const isInDB = await isUserInDB(id);
    if (!isInDB) {
      await ctx.reply(userNotInDB);
      await ctx.scene.leave();
      return;
    }

    accountsClass.setAccountID(ctx.from.id.toString());

    if (seeNode?.accountID && seeNode?.nodeName) {
      nodesClass.accountID = seeNode.accountID;
      this.showNode(ctx, seeNode.nodeName);
      return (seeNode = {});
    }

    showInUSD = await accountsClass.getShowInUSD();
    seeingOptions = false;
    nodesNames = null;

    if (seeAccount) {
      nodesClass.accountID = seeAccount;
      await nodesClass.showNodes(ctx, true, seeAccount, null, false, true);
      return (seeAccount = null);
    }

    this.showAccounts(ctx, false);
  } catch (err) {
    handleErr(err);
  }
};

a.actionSeeMoreOptions = function (ctx) {
  seeingOptions = true;
  this.actionSelect(ctx);
};

a.actionSeeLessOptions = function (ctx) {
  seeingOptions = false;
  this.actionSelect(ctx);
};

a.actionSelect = function (ctx) {
  this.showNode(ctx, ctx.update.callback_query.data.split("_")[1])
    .then(() => ctx.answerCbQuery())
    .catch((err) => handleErr(err, ctx));
};

a.actionChangeCurrency = async function (ctx) {
  try {
    const nodeName = ctx.update.callback_query.data.split("_")[1];
    showInUSD = await accountsClass.setShowInUSD(!showInUSD);

    await this.showNode(ctx, nodeName);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAccount = async function (ctx) {
  try {
    const accountID = ctx.update.callback_query.data.split("_")[1];
    nodesClass.accountID = accountID;
    nodesNames = await nodesClass.getNodesNames();

    await nodesClass.showNodes(ctx, true, accountID, null, false, true);
    await ctx.answerCbQuery();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAskDelete = async function (ctx) {
  try {
    const nodeName = ctx.update.callback_query.data.split("_")[1];

    await ctx.editMessageText(
      `Are you sure you want to delete *${nodeName}*?\n\n` +
        `This action *can't be undone*, unless you have a backup. To do a backup, use the /backup command.`,
      Extra.markup(
        Markup.inlineKeyboard([
          [Markup.callbackButton(`${SE} Yes, delete it ${SE}`, `delete_${nodeName}`)],
          [Markup.callbackButton(`${SE} No! Don't delete it ${SE}`, `backNode_${nodeName}`)],
        ])
      ).markdown()
    );
    await ctx.answerCbQuery();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionDelete = async function (ctx) {
  try {
    seeingOptions = false;
    const nodeName = ctx.update.callback_query.data.split("_")[1];

    const doc = await db.findOne({ _id: nodesClass.accountID });
    delete doc.nodes[nodeName];
    db.update({ _id: nodesClass.accountID }, { $set: { nodes: doc.nodes } }).catch((err) => handleErr(err, ctx));

    const publicKey = await nodesClass.getPublicKeyOfNode(nodeName);
    if (publicKey) {
      const doc = await db.findOne({ _id: "publicKeys" });

      delete doc[publicKey][nodesClass.accountID];
      if (Object.keys(doc[publicKey]).length === 0)
        db.update({ _id: "publicKeys" }, { $unset: { [publicKey]: true } }).catch((err) => handleErr(err, ctx));
      else
        db.update({ _id: "publicKeys" }, { $set: { [publicKey]: doc[publicKey] } }).catch((err) =>
          handleErr(err, ctx)
        );
    }

    nodesNames = await nodesClass.getNodesNames();
    await nodesClass.showNodes(ctx, true, nodesClass.accountID, null, false, true);
    await ctx.answerCbQuery(`The node was deleted from database.`);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionBackNode = function (ctx) {
  this.showNode(ctx, ctx.update.callback_query.data.split("_")[1])
    .then(() => ctx.answerCbQuery())
    .catch((err) => handleErr(err, ctx));
};

a.actionSettings = async function (ctx) {
  try {
    const accountID = ctx.update.callback_query.data.split("_")[1];
    const accountName = await accountsClass.getUserAccountsNames(ctx.from.id);

    await ctx.editMessageText(
      `The ID of "<u><b>${accountName[accountID]}</b></u>" is <code>${accountID}</code>. ` +
        "If you want, you can share this code with other people and they could import the account and see and modify the data as well.",
      Extra.markup(
        Markup.inlineKeyboard([
          [
            Markup.callbackButton(`\u2B05 Go back ${SE}`, "back"),
            Markup.callbackButton(`\u{1F5D1} Delete account ${SE}`, `askDeleteAccount_${accountID}`),
          ],
        ])
      ).HTML()
    );
    await ctx.answerCbQuery();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAskDeleteAccount = async function (ctx) {
  try {
    const accountID = ctx.update.callback_query.data.split("_")[1];

    const doc = await db.findOne({ _id: accountID });

    const persons = doc.accounts.length;
    let message;
    if (persons == 1)
      message =
        `Are you sure you want to delete *${doc.name}*?\n\n` +
        "Since you are the only person ussing this account, deleting it will permanently remove it from the database.\n\n" +
        "This action *can't be undone*, unless you have a backup. To do a backup, use the /backup command.";
    else
      message =
        `Are you sure you want to delete *${doc.name}*?\n\n` +
        `There are currently ${persons} persons using this account, including you. ` +
        "If you delete this account, it will only affect you, so for the other " +
        `${persons - 1 == 1 ? "person" : persons - 1 + " persons"} won't make any difference.\n\n` +
        "This action *can't be undone*, unless you have a backup. To do a backup, use the /backup command. " +
        `Or you could save the ID: \`${accountID}\`, and import it later, hoping the other ` +
        `person${persons - 1 == 1 ? "" : "s"} haven't deleted it also.`;

    await ctx.editMessageText(
      message,
      Extra.markup(
        Markup.inlineKeyboard([
          [
            Markup.callbackButton(
              `${SE} Yes, delete it ${persons > 1 ? "for me" : "permanently"} ${SE}`,
              `deleteAccount_${accountID}`
            ),
          ],
          [Markup.callbackButton(`${SE} No! Don't delte it ${SE}`, `settings_${accountID}`)],
        ])
      ).markdown()
    );
    await ctx.answerCbQuery();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionDeleteAccount = function (ctx) {
  const accountID = ctx.update.callback_query.data.split("_")[1];
  accountsClass
    .deleteAccount(accountID, ctx.from.id)
    .then(() => {
      this.showAccounts(ctx, true).then(() => ctx.answerCbQuery("Done! The account was deleted"));
    })
    .catch((err) => handleErr(err));
};

a.actionBack = function (ctx) {
  nodesClass
    .showNodes(ctx, true, nodesClass.accountID, null, false, true)
    .then(() => ctx.answerCbQuery())
    .catch((err) => handleErr(err, ctx));
};

a.actionAddNode = function (ctx, addNode) {
  addNode?.setAddNodeForAccount(nodesClass.accountID);
  ctx.scene.enter("addNode").catch((err) => handleErr(err, ctx));
};

a.actionAddPublicKey = function (ctx, addNode) {
  addNode?.setAddPublicKeyToNode(ctx.update.callback_query.data.split("_")[1], nodesClass.accountID);
  ctx.scene.enter("addNode").catch((err) => handleErr(err, ctx));
};

a.actionBackAccount = function (ctx) {
  this.showAccounts(ctx, true)
    .then(() => ctx.answerCbQuery())
    .catch((err) => handleErr(err, ctx));
};

a.actionAddEarning = function (ctx, addEarnings) {
  addEarnings?.setAddAnotherEntryFor(nodesClass.accountID, nodesClass.nodeName);
  ctx.scene.enter("addEarnings").catch((err) => handleErr(err, ctx));
};

a.setSeeNode = function (accountID, nodeName) {
  seeNode = { accountID: accountID, nodeName: nodeName };
};

a.setSeeAccount = function (accountID) {
  seeAccount = accountID;
};

exports.myNodes = a;
