/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const { db } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { userNotInDB, specialEmoji: SE } = require("../messages");
const { isUserInDB } = require("../isUserInDB");

const importAccount = {
  enter: async (ctx) => {
    try {
      const id = ctx.from.id.toString();
      const isInDB = await isUserInDB(id);
      if (!isInDB) {
        await ctx.deleteMessage();
        await ctx.reply(userNotInDB);
        await ctx.scene.leave();
        return;
      }

      if (ctx.update.callback_query) await ctx.answerCbQuery();

      const message = "To import an existing account, just send me the ID of the account.";
      const keyboard = Markup.inlineKeyboard([[Markup.callbackButton(`\u274C Cancel ${SE}`, "cancel")]]).extra();

      if (ctx.update.callback_query) await ctx.editMessageText(message, keyboard);
      else await ctx.reply(message, keyboard);
    } catch (err) {
      handleErr(err, ctx);
    }
  },

  hearsAll: async (ctx) => {
    try {
      const id = ctx.from.id.toString();
      const accountID = ctx.update.message.text;

      const account = await db.findOne({ _id: accountID });

      if (!account)
        return await ctx.reply(
          "That ID does not match any existing account.",
          Markup.inlineKeyboard([[Markup.callbackButton(`\u274C Cancel ${SE}`, "cancel")]]).extra()
        );

      await db.update({ _id: id }, { $addToSet: { accounts: accountID } }, { upsert: true });
      await db.update({ _id: accountID }, { $addToSet: { accounts: id } }, { upsert: true });

      await ctx.reply("Done! I've imported the account.", menu);
      await ctx.reply("\u{1F973}");
      await ctx.scene.leave();
    } catch (err) {
      handleErr(err, ctx);
    }
  },

  actionCancel: async (ctx) => {
    try {
      ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
      await ctx.deleteMessage();
      await ctx.reply("Ok. No new accounts for now.", menu);
      await ctx.scene.leave();
    } catch (err) {
      handleErr(err, ctx);
    }
  },

  onMessage: (ctx) =>
    ctx
      .reply(
        "Please send the ID of the existing account you want to import.",
        Markup.inlineKeyboard([[Markup.callbackButton(`\u274C Cancel ${SE}`, "cancel")]]).extra()
      )
      .catch((err) => handleErr(err, ctx)),
};

exports.importAccount = importAccount;
