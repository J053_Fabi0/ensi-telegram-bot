/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const Extra = require("telegraf/extra");

const { db, timeOut } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { isUserInDB } = require("../isUserInDB");
const { nodesStatus } = require("../nodesStatus/nodesStatus");
const { userNotInDB, noAccount, specialEmoji: SE } = require("../messages");

const Nodes = require("../classes/Nodes");
const Accounts = require("../classes/Accounts");

let nodeName = "";
let [askingForName, askingForPubKey] = [false, false];
let [addNodeForAccount, addPublicKeyToNode] = [null, null];

const nodesClass = new Nodes(db);
const accountsClass = new Accounts(db);

async function askSendName(ctx, editMessage) {
  try {
    askingForName = true;

    const message = "Let's add a new node. Please send its name.\n\nI could be anything, like `Node 1`.";
    const keyboard = Extra.markup(
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]])
    ).markdown();

    if (editMessage) await ctx.editMessageText(message, keyboard);
    else await ctx.reply(message, keyboard);
  } catch (err) {
    handleErr(err, ctx);
  }
}

async function askSendPubKey(ctx, editMessage, isReplacingPubKey = false) {
  try {
    askingForPubKey = true;
    askingForName = false;

    const message =
      "*Alright!*\n\n" +
      (isReplacingPubKey
        ? `Send me the new public key of *${nodeName}*.`
        : "If you want me to auto detect the earnings of this node, you can send me its public key.\n\n") +
      (addPublicKeyToNode
        ? ""
        : "You can skip this step and add every earning by yourself or set the public key later.");

    const keyboard = [];
    if (!addPublicKeyToNode) keyboard.push([Markup.callbackButton(`\u{23E9} Skip public key ${SE}`, "skip")]);
    keyboard.push([Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]);

    if (editMessage) await ctx.editMessageText(message, Extra.markup(Markup.inlineKeyboard(keyboard)).markdown());
    else await ctx.reply(message, Extra.markup(Markup.inlineKeyboard(keyboard)).markdown());
  } catch (err) {
    handleErr(err, ctx);
  }
}

const a = {};

let timeOutF = null;
a.leave = () => clearTimeout(timeOutF);
a.used = (ctx, next) => {
  clearTimeout(timeOutF);
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  next(ctx);
};

a.enter = async function (ctx) {
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  try {
    // Check if it is in the database
    const id = ctx.from.id.toString();
    const isInDB = await isUserInDB(id);
    if (!isInDB) {
      await ctx.reply(userNotInDB);
      await ctx.scene.leave();
      return;
    }

    // Check if in has any account
    const accounts = await accountsClass.getUserAccounts(ctx.from.id.toString());
    if (accounts.length === 0) {
      await ctx.reply(noAccount, menu);
      await ctx.scene.leave();
      return;
    }

    if (addNodeForAccount) {
      nodesClass.accountID = addNodeForAccount;
      addNodeForAccount = null;
      askSendName(ctx, true);
      return;
    }

    if (addPublicKeyToNode) {
      nodesClass.accountID = addPublicKeyToNode.accountID;
      nodeName = addPublicKeyToNode.nodeName;
      askingForPubKey = true;
      askSendPubKey(ctx, true, true);
      return;
    }

    if (accounts.length > 1) return await accountsClass.showAccounts(ctx, false, true, false);

    nodesClass.accountID = accounts[0];
    askSendName(ctx, false);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAccount = function (ctx) {
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

  const accountID = ctx.update.callback_query.data.split("_")[1];
  nodesClass.accountID = accountID;

  askSendName(ctx, true).catch((err) => handleErr(err, ctx));
};

a.actionCancel = async function (ctx, next) {
  try {
    next(ctx);

    askingForName = false;
    askingForPubKey = false;
    addPublicKeyToNode = null;
    addNodeForAccount = null;
    nodeName = "";
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionSkip = async function (ctx) {
  try {
    if (nodeName) await nodesClass.addNewNode(ctx, nodeName, true);

    nodeName = "";
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionMyNodes = function (ctx, myNodes) {
  myNodes.setSeeAccount(nodesClass.accountID);
  ctx.scene.enter("myNodes").catch((err) => handleErr(err, ctx));
};

a.hearsUnderline = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      'The name can\'t contain underlines "_"',
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};
a.hearsDot = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      'The name can\'t contain dots "."',
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};
a.hearsDolarSign = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      'The name can\'t start with a dolar sign "$"',
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};

a.hearsValidName = async function (ctx, next) {
  if (!askingForName) return next(ctx);

  try {
    const isNameInUse = await nodesClass.isNameInUse(ctx.update.message.text);
    if (isNameInUse) {
      await ctx.reply(
        "There is already a node with that name. Please, choose another name.",
        Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
      );
    } else {
      nodeName = ctx.update.message.text;
      askSendPubKey(ctx, false);
    }
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.hearsPublicKey = async function (ctx, next) {
  if (!askingForPubKey) return next(ctx);

  try {
    const publicKey = ctx.update.message.text;
    const exists = await nodesStatus.doesNodeExist(publicKey);

    if (!exists)
      return ctx
        .reply(
          "Oh... Sorry, but that public key is not valid or maybe the node is offline.\n\nPlease check that out and " +
            "try again (I'm still listening for a public key, unless you press cancel).",
          Markup.inlineKeyboard([
            [Markup.callbackButton(`\u{23E9} Skip public key ${SE}`, "skip")],
            [Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")],
          ]).extra()
        )
        .catch((err) => handleErr(err, ctx));

    const isPublicKeyUsedInAccount = await nodesClass.isPublicKeyUsedInAccount(publicKey);
    if (typeof isPublicKeyUsedInAccount === "string")
      return ctx
        .reply(
          `Ups... In this account, the node "${isPublicKeyUsedInAccount}" is already using that public key.\n` +
            "Please send another key or choose another account.",
          Markup.inlineKeyboard([
            [Markup.callbackButton(`\u{23E9} Skip public key ${SE}`, "skip")],
            [Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")],
          ]).extra()
        )
        .catch((err) => handleErr(err, ctx));

    nodesClass.addPublicKeyToNode(nodeName, publicKey);
    nodesClass.addNewNode(ctx, nodeName);
    nodeName = "";
    askingForPubKey = false;
    addPublicKeyToNode = null;
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.hearsText = function (ctx, next) {
  if (askingForName) {
    ctx
      .reply(
        "The name can't be longer than 10 characters. Please send a shorter name.",
        Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
      )
      .catch((err) => handleErr(err, ctx));
  } else if (askingForPubKey) {
    const keyboard = [];
    if (!addPublicKeyToNode) keyboard.push([Markup.callbackButton(`\u{23E9} Skip public key ${SE}`, "skip")]);
    keyboard.push([Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]);

    ctx
      .reply(
        "Sorry, that doesn’t seem to be a public key.\n" +
          "Public keys always start with 1 and have a length of either 50 or 51 characters.\n\n" +
          "You can find it inside the app: Keychains > Select your node's keychain > Public key.",
        Extra.markup(Markup.inlineKeyboard(keyboard)).markdown()
      )
      .catch((err) => handleErr(err, ctx));
  } else {
    next(ctx);
  }
};

a.onMessage = function (ctx, next) {
  if (askingForName || askingForPubKey)
    ctx
      .reply(
        "Only text messages, please.",
        Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
      )
      .catch((err) => handleErr(err, ctx));
  else next(ctx);
};

a.setAddNodeForAccount = function (accountID) {
  addNodeForAccount = accountID;
};

a.setAddPublicKeyToNode = function (nodeName, accountID) {
  addPublicKeyToNode = { nodeName: nodeName, accountID: accountID };
};

exports.addNode = a;
