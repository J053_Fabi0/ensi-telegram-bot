/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");
const DateFormat = require("../DateFormat");

const { db, timeOut } = require("../../main");
const { menu } = require("../keyboards");
const { getPRVPrice } = require("../PRVPrice");
const { handleErr } = require("../handleError");
const { isUserInDB } = require("../isUserInDB");
const { userNotInDB, noAccount, specialEmoji: SE } = require("../messages");

const Nodes = require("../classes/Nodes");
const Accounts = require("../classes/Accounts");

let accounts, prv_price, showInUSD, sortMethod;

const accountsClass = new Accounts(db);
const nodesClass = new Nodes(db);

async function enter(ctx, editMessage) {
  const id = ctx.from.id.toString();
  const isInDB = await isUserInDB(id);
  if (!isInDB) {
    if (editMessage) await ctx.editMessageText(userNotInDB);
    else await ctx.reply(userNotInDB);
    await ctx.scene.leave();
    return;
  }

  accounts = await accountsClass.getUserAccounts(ctx.from.id.toString());
  if (accounts.length == 0) {
    if (editMessage) await ctx.editMessageText(noAccount, menu);
    else await ctx.reply(noAccount, menu);
    await ctx.scene.leave();
    return;
  }

  showInUSD = await accountsClass.getShowInUSD();
  sortMethod = await accountsClass.getSortMethod();

  if (accounts.length > 1) {
    await accountsClass.showAccounts(ctx, editMessage, false, false);
  } else {
    nodesClass.accountID = accounts[0];
    await nodesClass.checkIfHasNodes(ctx, editMessage, async () => {
      showAccountStatistics(ctx, editMessage).catch((err) => handleErr(err, ctx));
    });
  }
}

async function showMessageForEarnings(ctx, editMessage, accountID = nodesClass.accountID) {
  try {
    const today = DateFormat.getOnlyDate();
    const nodesNames = await nodesClass.getNodesNames(accountID);

    const keyboard = Extra.markup(
      Markup.inlineKeyboard([
        [Markup.callbackButton(`${SE} Sort by last earning date \u{1F5D3}`, "sortByEarningDate")],
        [Markup.callbackButton(`\u2B05 Account statistics ${SE}`, `account_${nodesClass.accountID}`)],
      ])
    ).markdown();

    if (nodesNames.length === 0)
      return await ctx.editMessageText("You don't have any nodes in this account.", keyboard);

    // Nodes will be an array of arrays. Each element will have [name, earningsOfTheMonth]
    const nodes = [];
    for (const name of nodesNames) nodes.push([name, await nodesClass.getTotalEarningsForNode(today, name)]);

    // Sort nodes. The ones with more earnings at the top.
    nodes.sort((a, b) => b[1] - a[1]);

    const lengthOfLongestName = Math.max(...nodesNames.map((name) => name.length));
    const lengthOfLongestNum = Math.max(...nodes.map(([, a]) => a.toString().length));

    let message = `*Earnings this month*${SE}\n\``;
    // Add the separator (----...) at the top. The -3 is because the separator " | " has 3 characters and the +4 is because of the " PRV"
    for (let i = 0; i < lengthOfLongestNum + lengthOfLongestName + 3 + 4; i++) message += "-";
    message += "\n";

    // // Create the table
    for (let i = 0; i < nodes.length; i++) {
      message += `${nodes[i][1]} PRV`;

      // Add the spaces.
      const numberOfSpaces = lengthOfLongestNum - nodes[i][1].toString().length;
      for (let j = 0; j < numberOfSpaces; j++) message += " ";

      // Add the node name and the separator
      message += ` | ${nodes[i][0]}\n`;
    }
    message += "`";

    if (editMessage) await ctx.editMessageText(message, keyboard);
    else await ctx.reply(message, keyboard);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
}

async function showMessageForLastEarning(ctx, editMessage, accountID = nodesClass.accountID) {
  try {
    const nodes = await nodesClass.getLastEarningDateForAll(accountID);

    const keyboard = Extra.markup(
      Markup.inlineKeyboard([
        [Markup.callbackButton(`${SE} Sort by earnings \u{1F4B8}`, "sortByEarnings")],
        [Markup.callbackButton(`\u2B05 Account statistics ${SE}`, `account_${nodesClass.accountID}`)],
      ])
    ).markdown();

    if (nodes.length === 0)
      return await ctx.editMessageText("You don't have any nodes in this account.", keyboard);

    // This adds the days that have passed without earning to each element of the array.
    // So each element starts like [name, stringLastEarningDate], and ends like [name, stringLastEarningDate, daysSinceLastEarningDate]
    const today = DateFormat.getOnlyDate();

    for (let i = 0; i < nodes.length; i++)
      nodes[i].push(
        nodes[i][1] === 0 ? "No registered earnings" : DateFormat.getDaysFromDateToDate(today, nodes[i][1])
      );

    // Create an array with the information of every node
    const information = [];
    for (const node of nodes) {
      if (node[1] === 0) information.push("No data");
      else if (node[2] === 0) information.push("Today");
      else information.push(`${node[2]} day${node[2] === 1 ? "" : "s"}`);
    }

    const lengthOfLongestName = Math.max(...nodes.map(([a]) => a.length));
    const lengthOfLongestInfo = Math.max(...information.map((a) => a.length));

    let message = `*Time since last earning*${SE}\n\``;
    // Add the separator (----...) at the top. The -3 is because the separator " | " has 3 characters
    for (let i = 0; i < lengthOfLongestInfo + lengthOfLongestName + 3; i++) message += "-";
    message += "\n";

    // Create the table
    for (let i = 0; i < nodes.length; i++) {
      message += information[i];

      // Add the spaces.
      const numberOfSpaces = lengthOfLongestInfo - information[i].length;
      for (let j = 0; j < numberOfSpaces; j++) message += " ";

      // Add the node name and the separator
      message += ` | ${nodes[i][0]}\n`;
    }
    message += "`";

    if (editMessage) await ctx.editMessageText(message, keyboard);
    else await ctx.reply(message, keyboard);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
}

async function showAccountStatistics(ctx, editMessage) {
  if (showInUSD) prv_price = await getPRVPrice(ctx);

  const thisMonth = DateFormat.getOnlyDate();

  const total = await nodesClass.getTotalEarningsForAll();
  const average = await nodesClass.getAverageEarningsForAll();
  const times = await nodesClass.getHowManyTimesTheyEarned();

  const totalThisMonth = await nodesClass.getTotalEarningsForAll(thisMonth);
  const averageThisMonth = await nodesClass.getAverageEarningsForAll(thisMonth);
  const timesThisMonth = await nodesClass.getHowManyTimesTheyEarned(thisMonth);

  const { monthsInThePast } = await accountsClass.getSettings();
  let textForOtherMonths = "";
  for (let i = 1; i <= (monthsInThePast === undefined ? 2 : monthsInThePast); i++) {
    const lastMonth = DateFormat.getDateMinusMonths(i);

    const totalLastMonth = await nodesClass.getTotalEarningsForAll(lastMonth);
    const averageLastMonth = await nodesClass.getAverageEarningsForAll(lastMonth);
    const timesLastMonth = await nodesClass.getHowManyTimesTheyEarned(lastMonth);

    textForOtherMonths =
      textForOtherMonths +
      `\n\n<b>Total earnings ${i} month${i > 1 ? "s" : ""} ago</b>` +
      "\n· <b>Total:</b> <code>" +
      (showInUSD ? Number((totalLastMonth * prv_price).toFixed(4)) : totalLastMonth) +
      `</code> ${showInUSD ? "USD" : "PRV"}.` +
      `\n· <b>Times they earned:</b> <code>${timesLastMonth}</code>.` +
      "\n· <b>Average earning:</b> <code>" +
      (showInUSD ? Number((averageLastMonth * prv_price).toFixed(4)) : averageLastMonth) +
      `</code> ${showInUSD ? "USD" : "PRV"}.`;
  }

  const message =
    (accounts.length > 1 ? `<b><u>${nodesClass.accountName}</u></b>` : "") +
    "\n\n<b>Total earnings this month</b>" +
    "\n· <b>Total:</b> <code>" +
    `${showInUSD ? Number((totalThisMonth * prv_price).toFixed(4)) : totalThisMonth}</code> ${
      showInUSD ? "USD" : "PRV"
    }.` +
    `\n· <b>Times they earned:</b> <code>${timesThisMonth}</code>.` +
    "\n· <b>Average earning:</b> <code>" +
    `${showInUSD ? Number((averageThisMonth * prv_price).toFixed(4)) : averageThisMonth}</code> ${
      showInUSD ? "USD" : "PRV"
    }.` +
    textForOtherMonths +
    "\n\n<b>Total earnings since the begining until now</b>" +
    "\n· <b>Total:</b> <code>" +
    `${showInUSD ? Number((total * prv_price).toFixed(4)) : total}</code> ${showInUSD ? "USD" : "PRV"}.` +
    `\n· <b>Times they earned:</b> <code>${times}</code>.` +
    "\n· <b>Average earning:</b> <code>" +
    `${showInUSD ? Number((average * prv_price).toFixed(4)) : average}</code> ${showInUSD ? "USD" : "PRV"}.` +
    `${showInUSD ? "\n\nThe PRV is at <code>" + prv_price + "</code> USD." : ""}`;

  const currencyBtn = Markup.callbackButton(
    `${SE} Currency: ${showInUSD ? "USD" : "PRV"} ${SE}`,
    "changeCurrency"
  );
  const keyboard =
    accounts.length > 1
      ? Extra.markup(
          Markup.inlineKeyboard([
            [Markup.callbackButton(`\u2B05 Go back ${SE}`, "backAccount"), currencyBtn],
            [Markup.callbackButton(`${SE} Node statistics \u27A1`, "showNodesStatistics")],
          ])
        ).HTML()
      : Extra.markup(
          Markup.inlineKeyboard([
            [currencyBtn],
            [Markup.callbackButton(`${SE} Node statistics \u27A1`, "showNodesStatistics")],
          ])
        ).HTML();

  if (editMessage) await ctx.editMessageText(message, keyboard);
  else await ctx.reply(message, keyboard);
}

const a = {};

let timeOutF = null;
a.leave = () => clearTimeout(timeOutF);
a.used = (ctx, next) => {
  clearTimeout(timeOutF);
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  next(ctx);
};

a.enter = function (ctx) {
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  enter(ctx, false).catch((err) => handleErr(err, ctx));
};

a.actionShowNodesStatistics = function (ctx) {
  if (sortMethod === "earningsOfTheMonth") showMessageForEarnings(ctx, true);
  else showMessageForLastEarning(ctx, true);
};

a.actionSortByLastEarning = function (ctx) {
  showMessageForLastEarning(ctx, true);
  accountsClass.setSortMethod("lastEarningDate").catch((err) => handleErr(err, ctx));
};

a.actionSortByEarnings = function (ctx) {
  showMessageForEarnings(ctx, true);
  accountsClass.setSortMethod("earningsOfTheMonth").catch((err) => handleErr(err, ctx));
};

a.actionAccount = async function (ctx) {
  try {
    const accountID = ctx.update.callback_query.data.split("_")[1];
    nodesClass.accountID = accountID;

    await showAccountStatistics(ctx, true);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionChangeCurrency = async function (ctx) {
  try {
    showInUSD = await accountsClass.setShowInUSD(!showInUSD);

    await showAccountStatistics(ctx, true).then(() => ctx.answerCbQuery().catch((err) => handleErr(err, ctx)));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionBackAccount = async function (ctx) {
  try {
    await enter(ctx, true);
    await ctx.answerCbQuery();
  } catch (err) {
    handleErr(err, ctx);
  }
};

exports.showStatistics = a;
