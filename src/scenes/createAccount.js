/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");

const { db } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { isUserInDB } = require("../isUserInDB");
const { userNotInDB, specialEmoji: SE } = require("../messages");

let accountID = null;
let askingForName = true;

const a = {};

a.enter = async function (ctx) {
  try {
    const id = ctx.from.id.toString();
    const isInDB = await isUserInDB(id);
    if (!isInDB) {
      await ctx.deleteMessage();
      await ctx.reply(userNotInDB);
      await ctx.scene.leave();
      return;
    }

    if (ctx.update.callback_query) ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

    const message =
      "To create a new account, simply send me how are you going to name it.\n\n" +
      "It can be anything you want, for example, `Account 1`.";
    const keyboard = Extra.markup(Markup.inlineKeyboard([Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")])).markdown();

    askingForName = true;

    if (ctx.update.callback_query) await ctx.editMessageText(message, keyboard);
    else await ctx.reply(message, keyboard);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAddNode = function (ctx, addNode) {
  addNode?.setAddNodeForAccount(accountID);
  ctx.scene.enter("addNode");
};

a.actionCancel = async (ctx) => {
  try {
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
    await ctx.deleteMessage();
    await ctx.reply("Ok. No new accounts for now.", menu);
    await ctx.scene.leave();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.hearsUnderline = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      'The name can\'t contain underlines "_"',
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};
a.hearsDot = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      'The name can\'t contain dots "."',
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};
a.hearsDolarSign = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      'The name can\'t start with a dolar sign "$"',
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};

a.hearsValidName = async function (ctx, next) {
  if (!askingForName) return next(ctx);
  try {
    const id = ctx.from.id.toString();
    const doc = await db.insert({ accounts: [id], name: ctx.update.message.text, nodes: {} });

    await db.update({ _id: id }, { $addToSet: { accounts: doc._id } }, { upsert: true });

    const keyboard = Extra.markup(
      Markup.inlineKeyboard([Markup.callbackButton(`\u2795 Add its first node ${SE}`, "addNode")])
    ).markdown();
    await ctx.reply(
      `There we go. I've created a new account named *${ctx.update.message.text}*.` +
        ` Its ID is \`${doc._id}\`, use it to share the account.`,
      keyboard
    );

    accountID = doc._id;
    askingForName = false;
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.hearsLongerName = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx.reply("The name can't be longer than 32 characters.").catch((err) => handleErr(err, ctx));
};

a.onMessage = function (ctx, next) {
  if (!askingForName) return next(ctx);
  ctx
    .reply(
      "Please, send the name of the new account you'r going to create.",
      Markup.inlineKeyboard([[Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};

exports.createAccount = a;
