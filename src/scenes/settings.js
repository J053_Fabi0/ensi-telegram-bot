/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const Extra = require("telegraf/extra");

const { db, timeOut } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { isUserInDB } = require("../isUserInDB");
const { userNotInDB, specialEmoji: SE } = require("../messages");

const Accounts = require("../classes/Accounts");
const accountsClass = new Accounts(db);

async function showSettingsMenu(editMessage, ctx) {
  const id = ctx.from.id.toString();

  const userSettings = await accountsClass.getSettings(id);
  const userName = await accountsClass.getUserName();

  const message = `Settings for _${userName}_'s profile.`;
  const keyboard = Extra.markup(
    Markup.inlineKeyboard([
      [
        Markup.callbackButton(
          `${SE} Months in the past: ${
            (userSettings.monthsInThePast === undefined ? 2 : userSettings.monthsInThePast) || "dissabled"
          } ${SE}`,
          "change_months"
        ),
      ],
      [Markup.callbackButton(`\u274C Close ${SE}`, "close")],
    ])
  ).markdown();

  if (editMessage) await ctx.editMessageText(message, keyboard);
  else await ctx.reply(message, keyboard);
}

async function showChangeMonth(ctx) {
  const id = ctx.from.id.toString();
  const userSettings = await accountsClass.getSettings(id);

  const buttons = [];
  if ((userSettings.monthsInThePast === undefined ? 2 : userSettings.monthsInThePast) > 0)
    buttons.push(Markup.callbackButton("-1", "minusMonth"));
  if ((userSettings.monthsInThePast === undefined ? 2 : userSettings.monthsInThePast) < 10)
    buttons.push(Markup.callbackButton("+1", "plusMonth"));

  await ctx.editMessageText(
    "Number of last months to see in general and individual nodes statistics: " +
      `<b><u>${
        (userSettings.monthsInThePast === undefined ? 2 : userSettings.monthsInThePast) || "dissabled"
      }</u></b>.`, // If it's 0, it will show dissabled
    Extra.markup(Markup.inlineKeyboard([buttons, [Markup.callbackButton(`\u2705 Ok ${SE}`, "goMenu")]])).HTML()
  );
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
}

const a = {};

let timeOutF = null;
a.leave = () => clearTimeout(timeOutF);
a.used = (ctx, next) => {
  clearTimeout(timeOutF);
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  next(ctx);
};

a.enter = async (ctx) => {
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  try {
    const id = ctx.update.message.from.id.toString();
    const isInDB = await isUserInDB(id);

    if (!isInDB) {
      await ctx.reply(userNotInDB);
      return await ctx.scene.leave();
    }

    await showSettingsMenu(false, ctx);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionChangeMonths = (ctx) => {
  showChangeMonth(ctx).catch((err) => handleErr(err, ctx));
};

a.actionAddOrRestMoths = async (ctx) => {
  try {
    const id = ctx.from.id.toString();
    const userSettings = await accountsClass.getSettings(id);

    if (ctx.match === "plusMonth")
      userSettings.monthsInThePast =
        (userSettings.monthsInThePast === undefined ? 2 : userSettings.monthsInThePast) + 1;
    else
      userSettings.monthsInThePast =
        (userSettings.monthsInThePast === undefined ? 2 : userSettings.monthsInThePast) - 1;

    accountsClass.setSettings(userSettings, id);

    await showChangeMonth(ctx);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionGoMenu = async (ctx) => {
  try {
    await showSettingsMenu(true, ctx);
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionClose = async (ctx) => {
  try {
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
    await ctx.deleteMessage();
    await ctx.reply(`${SE || "🏠"}`, menu);
    await ctx.scene.leave();
  } catch (err) {
    handleErr(err, ctx);
  }
};

exports.settings = a;
