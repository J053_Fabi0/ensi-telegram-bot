/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const Extra = require("telegraf/extra");
const { db, timeOut } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const DateFormat = require("../DateFormat");
const { specialEmoji: SE } = require("../messages");

const Nodes = require("../classes/Nodes");
const Accounts = require("../classes/Accounts");

let addAnotherEntryFor = {};
let askingForSomething = false;
let date = null;

const nodesClass = new Nodes(db);
const accountsClass = new Accounts(db);

const a = {};

// This regex looks for the date and the earning
a.dateRegEx = /^((20)?(\d{2})[-/ ])?(\d{1,2})[-/ ](\d{1,2})([-/ ](\d+)([.,]\d+)?)$/;

let timeOutF = null;
a.leave = () => clearTimeout(timeOutF);
a.used = (ctx, next) => {
  clearTimeout(timeOutF);
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  next(ctx);
};

a.enter = async function (ctx) {
  timeOutF = setTimeout(() => ctx.scene.leave(), timeOut);
  try {
    // If the object of add addAnotherEntryFor has the data for which node we want to add an entry
    if (addAnotherEntryFor?.accountID && addAnotherEntryFor?.nodeName) {
      nodesClass.accountID = addAnotherEntryFor.accountID;
      nodeSelected(ctx, addAnotherEntryFor.nodeName);
      return (addAnotherEntryFor = null);
    }

    await accountsClass.showAccounts(ctx, false, true, true, "Select the node which earned", true);

    // If you only have one account, it should be assigned to the nodesClass constructor
    const accounts = await accountsClass.getUserAccounts(ctx.from.id.toString());
    if (accounts && accounts.length == 1) nodesClass.accountID = accounts[0];
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionShowNode = async (ctx, myNodes) => {
  try {
    myNodes?.setSeeNode(nodesClass.accountID, nodesClass.nodeName);
    await ctx.scene.enter("myNodes");
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionAccount = async (ctx) => {
  try {
    await ctx.answerCbQuery();

    const accountID = ctx.update.callback_query.data.split("_")[1];
    nodesClass.accountID = accountID;

    await nodesClass.showNodes(ctx, true, accountID, "Select the node which earned", true);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionSelect = function (ctx) {
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

  const nodeName = ctx.update.callback_query.data.split("_")[1];
  nodeSelected(ctx, nodeName);
};

const nodeSelected = (ctx, nodeName) => {
  nodesClass.nodeName = nodeName;
  const message = `Now tell me how much did *${nodeName}* earned today (\`${DateFormat.getOnlyDate()}\`).\n`;
  const keyboard = Extra.markup(
    Markup.inlineKeyboard([
      [
        Markup.callbackButton(`\u2B05 Go back ${SE}`, "back"),
        Markup.callbackButton(`\u{1F5D3} Another day ${SE}`, `anotherDay_${nodeName}`),
      ],
      [Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")],
    ])
  ).markdown();
  askingForSomething = true;

  ctx.editMessageText(message, keyboard).catch((err) => handleErr(err, ctx));

  date = null;
};

a.actionAnotherDay = async (ctx) => {
  try {
    const message =
      `Now tell me how much did *${
        ctx.update.callback_query.data.split("_")[1]
      }* earned today (\`${DateFormat.getOnlyDate()}\`).` +
      "\n\nTo specify a day, you'll need to send it in this format: `YY-MM-DD`, or just `MM-DD` to take the actual year, *followed by how much it earned*. The separator could be a space or a slash also." +
      "\n\nFor example, if today it earned *32* PRV, you could wirte it like this:\n" +
      `\`${DateFormat.getOnlyDate(null, false, true)} 32\` or` +
      ` \`${DateFormat.getOnlyDate(null, false, true).replace(/-/g, " ")} 32\` or` +
      ` \`${DateFormat.getOnlyDate(null, false, true).replace(/-/g, "/")} 32\`.` +
      "\nBut you could also skip the year to use the actual actual one:\n" +
      `\`${DateFormat.getOnlyDate(null, true)} 32\` or` +
      ` \`${DateFormat.getOnlyDate(null, true).replace(/-/g, " ")} 32\` or` +
      ` \`${DateFormat.getOnlyDate(null, true).replace(/-/g, "/")} 32\`.` +
      "\n\nIf you only send the earning, the earning will be added for today. So, just saying `32` would be the same as the above examples.";

    const keyboard = Extra.markup(
      Markup.inlineKeyboard([
        [Markup.callbackButton(`\u2B05 Go back ${SE}`, "back")],
        [Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")],
      ])
    ).markdown();

    await ctx
      .editMessageText(message, keyboard)
      .then(() => ctx.answerCbQuery().catch((err) => handleErr(err, ctx)));
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.actionBack = (ctx) => {
  askingForSomething = false;
  nodesClass
    .showNodes(ctx, true, null, "Select the node which earned", true)
    .then(() => ctx.answerCbQuery().catch((err) => handleErr(err, ctx)))
    .catch((err) => handleErr(err, ctx));
};

a.actionBackAccount = (ctx) =>
  accountsClass
    .showAccounts(ctx, true, true)
    .then(() => ctx.answerCbQuery())
    .catch((err) => handleErr(err, ctx));

a.hearsFullDate = async function (ctx, next) {
  try {
    if (!askingForSomething) return next(ctx);
    const dateProvided = ctx.update.message.text;

    /*
     * This array contains the groups of the regex. These are the indexes I care about:
     * 3 - year without the first 20: 2021 would be just 21
     * 4 - month
     * 5 - day
     * 6 - earning
     */
    const array = [...dateProvided.match(a.dateRegEx)];

    // Make them all numbers
    for (let i = 3; i <= 5; i++) array[i] = Number(array[i]);

    // Format them as YYYY-MM-DD
    array[3] = array[3] ? "20" + (array[3] <= 9 ? `0${array[3]}` : array[3]) : DateFormat.getActualYear();
    array[4] = array[4] <= 9 ? `0${array[4]}` : array[4];
    array[5] = array[5] <= 9 ? `0${array[5]}` : array[5];
    date = `${array[3]}-${array[4]}-${array[5]}`;

    // For the earning, make write a replace the possible coma by a dot and make it a number
    array[6] = Number(array[6].toString().replace(",", "."));

    if (!DateFormat.checkIfValidDateArr([array[3], array[4], array[5]]))
      return await ctx.reply(
        "The format is correct, but the day is not.\nMaybe you set 31 days to a month that has only 30, or you tried a date in the future.",
        Markup.inlineKeyboard([
          Markup.callbackButton(`\u2B05 Go back ${SE}`, "back"),
          Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel"),
        ]).extra()
      );

    await this.addEarning(ctx, date, array[6]);
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.hearsNumber = async function (ctx, next) {
  if (!askingForSomething) return next(ctx);
  const earning = Number(ctx.update.message.text.replace(",", "."));
  const keyboard = Markup.inlineKeyboard([
    Markup.callbackButton(`\u2B05 Go back ${SE}`, "back"),
    Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel"),
  ]).extra();

  const ifWasMistake = "\n\nIf that was a mistake, you can send the correct value again.";
  if (earning === 0)
    return await ctx.reply(
      "Zero earnings? If it didn't earned something, then just click Cancel \u{1F937}." + ifWasMistake,
      keyboard
    );

  if (earning >= 1_000_000 && earning < 100_000_000)
    return await ctx.reply(
      `Are you saying your node earned ${(
        (earning / 100_000_000) *
        100
      ).toFixed()}% of the max supply? That's a little too much, don't you think? \u{1F914}.${ifWasMistake}`,
      keyboard
    );

  if (earning >= 100_000_000)
    return await ctx.reply(
      `Bro..., the max supply of PRV will be 100,000,000, and you are saying your node earned ${earning}? \u{1F928}.${ifWasMistake}`,
      keyboard
    );

  if (earning > 0 && earning <= 0.0000001)
    return await ctx.reply(
      "Are you sure that's an earning? That looks more like the fee you have paid for the transaction \u{1F606}." +
        "\n\nIf that was a mistake, you can send the correct value again.",
      keyboard
    );

  this.addEarning(ctx, date, ctx.update.message.text.replace(",", "."));
};

a.addEarning = (ctx, date = null, earning) => {
  nodesClass
    .setNewEntry(earning, date)
    .then(async () => {
      await ctx.replyWithMarkdown(
        `Great! I've added that *${nodesClass.nodeName}* earned *${earning}* PRV` +
          `${date ? " in *" + date + "*" : " today"}.`,
        Markup.inlineKeyboard([
          [Markup.callbackButton(`\u{1F502} Another one for this node ${SE}`, "repeat")],
          [Markup.callbackButton(`➕ Add another entry ${SE}`, "addEarning")],
          [Markup.callbackButton(`\u{1F5C3} Show node ${SE}`, "showNode")],
          [Markup.callbackButton(`\u{1F3E0} Open keyboard menu ${SE}`, "leave")],
        ]).extra()
      );

      askingForSomething = false;
    })
    .catch((err) => handleErr(err));
};

a.onMessage = function (ctx, next) {
  if (!askingForSomething) return next(ctx);
  this.replyThatUsedBadFormat(ctx);
};

a.replyThatUsedBadFormat = (ctx) => {
  ctx
    .replyWithMarkdown(
      "Please use the format described above. You can see it by pressing the _\u{1F5D3} Another day_ button.",
      Markup.inlineKeyboard([
        [
          Markup.callbackButton(`\u2B05 Go back ${SE}`, "back"),
          Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel"),
        ],
      ]).extra()
    )
    .catch((err) => handleErr(err, ctx));
};

a.goBackNode = function (ctx) {
  ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

  this.setAddAnotherEntryFor(nodesClass.accountID, nodesClass.nodeName);

  ctx.scene.enter("addEarnings").catch((err) => handleErr(err, ctx));
};

a.cancelOperation = async (ctx) => {
  try {
    askingForSomething = false;
    ctx.answerCbQuery().catch((err) => handleErr(err, ctx));

    await ctx.deleteMessage();
    await ctx.reply("Adding new entry canceled.", menu);
    await ctx.scene.leave();
  } catch (err) {
    handleErr(err, ctx);
  }
};

a.setAddAnotherEntryFor = (accountID, nodeName) =>
  (addAnotherEntryFor = { accountID: accountID, nodeName: nodeName });

exports.addEarnings = a;
