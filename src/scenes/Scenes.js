/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Scene = require("telegraf/scenes/base");

////////////////////////////////////////
////////// addEarningsScene ////////////
////////////////////////////////////////
const { addEarnings } = require("./addEarnings");
const addEarningsScene = new Scene("addEarnings");

addEarningsScene.leave(() => addEarnings.leave());
addEarningsScene.enter((ctx) => addEarnings.enter(ctx));
addEarningsScene.on(["message", "callback_query"], (ctx, next) => addEarnings.used(ctx, next));
addEarningsScene.action("back", (ctx) => addEarnings.actionBack(ctx));
addEarningsScene.action("repeat", (ctx) => addEarnings.goBackNode(ctx));
addEarningsScene.action(/^select_/, (ctx) => addEarnings.actionSelect(ctx));
addEarningsScene.action("cancel", (ctx) => addEarnings.cancelOperation(ctx));
addEarningsScene.action(/^account_/, (ctx) => addEarnings.actionAccount(ctx));
addEarningsScene.action(/^anotherDay_/, (ctx) => addEarnings.actionAnotherDay(ctx));
addEarningsScene.action("backAccount", (ctx) => addEarnings.actionBackAccount(ctx));
addEarningsScene.action("showNode", (ctx) => addEarnings.actionShowNode(ctx, myNodes));

addEarningsScene.hears(addEarnings.dateRegEx, (ctx, next) => addEarnings.hearsFullDate(ctx, next));
addEarningsScene.hears(/^\d*([.,]\d+)?$/, (ctx, next) => addEarnings.hearsNumber(ctx, next));

addEarningsScene.on("message", (ctx, next) => addEarnings.onMessage(ctx, next));

exports.addEarningsScene = addEarningsScene;

////////////////////////////////////////
//////////// myNodesScene //////////////
////////////////////////////////////////
const { myNodes } = require("./myNodes");
const myNodesScene = new Scene("myNodes");

myNodesScene.leave(() => myNodes.leave());
myNodesScene.enter((ctx) => myNodes.enter(ctx));
myNodesScene.on(["message", "callback_query"], (ctx, next) => myNodes.used(ctx, next));

myNodesScene.action("back", (ctx) => myNodes.actionBack(ctx));
myNodesScene.action(/select_/, (ctx) => myNodes.actionSelect(ctx));
myNodesScene.action(/delete_/, (ctx) => myNodes.actionDelete(ctx));
myNodesScene.action(/account_/, (ctx) => myNodes.actionAccount(ctx));
myNodesScene.action(/backNode_/, (ctx) => myNodes.actionBackNode(ctx));
myNodesScene.action(/settings_/, (ctx) => myNodes.actionSettings(ctx));
myNodesScene.action(/askDelete_/, (ctx) => myNodes.actionAskDelete(ctx));
myNodesScene.action("backAccount", (ctx) => myNodes.actionBackAccount(ctx));
myNodesScene.action("addNode", (ctx) => myNodes.actionAddNode(ctx, addNode));
myNodesScene.action(/deleteAccount_/, (ctx) => myNodes.actionDeleteAccount(ctx));
myNodesScene.action(/seeMoreOptions_/, (ctx) => myNodes.actionSeeMoreOptions(ctx));
myNodesScene.action(/seeLessOptions_/, (ctx) => myNodes.actionSeeLessOptions(ctx));
myNodesScene.action(/changeCurrency_/, (ctx) => myNodes.actionChangeCurrency(ctx));
myNodesScene.action("addEarning", (ctx) => myNodes.actionAddEarning(ctx, addEarnings));
myNodesScene.action(/askDeleteAccount_/, (ctx) => myNodes.actionAskDeleteAccount(ctx));
myNodesScene.action(/addPublicKey_/, (ctx) => myNodes.actionAddPublicKey(ctx, addNode));

exports.myNodesScene = myNodesScene;
exports[Symbol.for("myNodes")] = myNodes;

////////////////////////////////////////
//////////// addNodeScene //////////////
////////////////////////////////////////
const { addNode } = require("./addNode");
const addNodeScene = new Scene("addNode");

addNodeScene.leave(() => addNode.leave());
addNodeScene.enter((ctx) => addNode.enter(ctx));
addNodeScene.on(["message", "callback_query"], (ctx, next) => addNode.used(ctx, next));
addNodeScene.action("skip", (ctx) => addNode.actionSkip(ctx));
addNodeScene.action(/^account_/, (ctx) => addNode.actionAccount(ctx));
addNodeScene.action("myNodes", (ctx) => addNode.actionMyNodes(ctx, myNodes));
addNodeScene.action("cancel", (ctx, next) => addNode.actionCancel(ctx, next));

addNodeScene.hears(/\./, (ctx, next) => addNode.hearsDot(ctx, next));
addNodeScene.hears(/_/, (ctx, next) => addNode.hearsUnderline(ctx, next));
addNodeScene.hears(/^\$/, (ctx, next) => addNode.hearsDolarSign(ctx, next));
addNodeScene.hears(/^.{1,10}$/, (ctx, next) => addNode.hearsValidName(ctx, next));
addNodeScene.hears(/^1.{49,50}$/, (ctx, next) => addNode.hearsPublicKey(ctx, next));
addNodeScene.hears(/./, (ctx, next) => addNode.hearsText(ctx, next));

addNodeScene.on("message", (ctx, next) => addNode.onMessage(ctx, next));

exports.addNodeScene = addNodeScene;
exports[Symbol.for("addNode")] = addNode;

////////////////////////////////////////
//////////// settingsScene /////////////
////////////////////////////////////////
const { settings } = require("./settings");
const settingsScene = new Scene("settings");

settingsScene.leave(() => settings.leave());
settingsScene.enter((ctx) => settings.enter(ctx));
settingsScene.on(["message", "callback_query"], (ctx, next) => settings.used(ctx, next));
settingsScene.action("close", (ctx) => settings.actionClose(ctx));
settingsScene.action("goMenu", (ctx) => settings.actionGoMenu(ctx));
settingsScene.action("change_months", (ctx) => settings.actionChangeMonths(ctx));
settingsScene.action(["plusMonth", "minusMonth"], (ctx) => settings.actionAddOrRestMoths(ctx));

exports.settingsScene = settingsScene;

////////////////////////////////////////
///////////// startScene ///////////////
//////////// setNameScene //////////////
////////////////////////////////////////
const { start } = require("./start");
const startScene = new Scene("start");
startScene.enter((ctx) => start.enter(ctx));

const { setName } = require("./start");
const setNameScene = new Scene("setName");
setNameScene.hears(/^.{1,32}$/, (ctx) => setName.hearsValidName(ctx));
setNameScene.hears(/.{33,}/, (ctx) => setName.hearsLongerName(ctx));
setNameScene.on("message", (ctx) => setName.onMessage(ctx));

exports.startScene = startScene;
exports.setNameScene = setNameScene;

////////////////////////////////////////
////////// createAccountScene //////////
////////////////////////////////////////
const { createAccount } = require("./createAccount");
const createAccountScene = new Scene("createAccount");

createAccountScene.enter((ctx) => createAccount.enter(ctx));

createAccountScene.action("cancel", (ctx) => createAccount.actionCancel(ctx));
createAccountScene.action("addNode", (ctx) => createAccount.actionAddNode(ctx, addNode));

createAccountScene.hears(/_/, (ctx, next) => createAccount.hearsUnderline(ctx, next));
createAccountScene.hears(/^\$/, (ctx, next) => createAccount.hearsDolarSign(ctx, next));
createAccountScene.hears(/\./, (ctx, next) => createAccount.hearsDot(ctx, next));
createAccountScene.hears(/^.{1,32}$/, (ctx, next) => createAccount.hearsValidName(ctx, next));
createAccountScene.hears(/.{33,}/, (ctx, next) => createAccount.hearsLongerName(ctx, next));

createAccountScene.on("message", (ctx, next) => createAccount.onMessage(ctx, next));

exports.createAccountScene = createAccountScene;

////////////////////////////////////////
////////// importAccountScene //////////
////////////////////////////////////////
const { importAccount } = require("./importAccount");
const importAccountScene = new Scene("importAccount");

importAccountScene.enter((ctx) => importAccount.enter(ctx));
importAccountScene.hears(/.*/, (ctx) => importAccount.hearsAll(ctx));
importAccountScene.action("cancel", (ctx) => importAccount.actionCancel(ctx));
importAccountScene.on("message", (ctx) => importAccount.onMessage(ctx));

exports.importAccountScene = importAccountScene;

////////////////////////////////////////
///////// showStatisticsScene //////////
////////////////////////////////////////
const { showStatistics } = require("./showStatistics");
const showStatisticsScene = new Scene("showStatistics");

showStatisticsScene.leave(() => showStatistics.leave());
showStatisticsScene.enter((ctx) => showStatistics.enter(ctx));
showStatisticsScene.on(["message", "callback_query"], (ctx, next) => showStatistics.used(ctx, next));
showStatisticsScene.action(/^account_/, (ctx) => showStatistics.actionAccount(ctx));
showStatisticsScene.action("backAccount", (ctx) => showStatistics.actionBackAccount(ctx));
showStatisticsScene.action("changeCurrency", (ctx) => showStatistics.actionChangeCurrency(ctx));
showStatisticsScene.action("sortByEarnings", (ctx) => showStatistics.actionSortByEarnings(ctx));
showStatisticsScene.action("sortByEarningDate", (ctx) => showStatistics.actionSortByLastEarning(ctx));
showStatisticsScene.action("showNodesStatistics", (ctx) => showStatistics.actionShowNodesStatistics(ctx));

exports.showStatisticsScene = showStatisticsScene;
