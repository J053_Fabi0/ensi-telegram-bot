/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");

const { db, timeOut } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { isUserInDB } = require("../isUserInDB");
const { startMessage, whatIsAnAccount, specialEmoji: SE } = require("../messages");

const start = {
  enter: async (ctx) => {
    try {
      const id = ctx.update.message.from.id.toString();
      const isInDB = await isUserInDB(id);

      if (isInDB) {
        await ctx.reply(startMessage, menu);
        return await ctx.scene.leave();
      }

      await ctx.reply(startMessage);
      await ctx.replyWithMarkdown(
        "*How should I call you?* Please send your name or nickname. It can be anything you want."
      );
      await ctx.scene.enter("setName");
    } catch (err) {
      handleErr(err, ctx);
    }
  },
};
exports.start = start;

// ##################
// ## setNameScene ##
// ##################
const setName = {
  hearsValidName: async (ctx) => {
    try {
      const id = ctx.update.message.from.id.toString();
      await db.insert({ _id: id, accounts: [], name: ctx.update.message.text });

      await ctx.replyWithMarkdown("*You are now registered!*");
      await ctx.reply("\u{1F973}");
      await ctx.replyWithMarkdown(
        "*Now you should either create a new account for a set of nodes, or import an existing one if a friend gives you the key.*" +
          whatIsAnAccount +
          "\n\nYou will also be able to create or import as many accounts as you need at any time using the /addaccount command.",
        Markup.inlineKeyboard([
          [Markup.callbackButton(`\u2795 Create new account ${SE}`, "create")],
          [Markup.callbackButton(`\u2B07 Import account ${SE}`, "import")],
        ]).extra()
      );

      await ctx.scene.leave();
    } catch (err) {
      handleErr(err, ctx);
    }
  },

  hearsLongerName: (ctx) =>
    ctx.reply("Your name can't be longer than 32 characters.").catch((err) => handleErr(err, ctx)),

  onMessage: (ctx) => ctx.reply("Send your name or nickname, please.").catch((err) => handleErr(err, ctx)),
};

exports.setName = setName;
