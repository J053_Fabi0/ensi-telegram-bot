/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const axios = require("axios");
const { db } = require("../main");
const DateFormat = require("./DateFormat");

module.exports = {
  /**
   * Returns the PRV price taken from incscan.io. Give ctx if you want to replyWithChatAction("typing") when a reload is necessary.
   * @returns number
   */
  getPRVPrice: async function (ctx = null) {
    let prv_price = await db.findOne({ _id: "prv_price" });
    const date = new Date();
    const minHoursBeforeReloadPrice = 1.5;

    if (!prv_price) {
      await db.update({ _id: "prv_price" }, { $set: { lastPRVPriceDate: date.toISOString() } }, { upsert: true });
      prv_price = { lastPRVPriceDate: date.toISOString() };
    }

    const hoursSinceLastUpdate = Math.abs(date - DateFormat.getDateFromString(prv_price.lastPRVPriceDate)) / 3600000;

    if (!prv_price.prv_price || hoursSinceLastUpdate >= minHoursBeforeReloadPrice) {
      await ctx?.replyWithChatAction("typing");
      const res = await axios.get("https://api.incscan.io/pdex/market");

      if (!res && !res.data && !res.data.lastPRVPrice) throw "Incscan API has no data. \u{1F627}";
      await db.update({ _id: "prv_price" }, { $set: { prv_price: res.data.lastPRVPrice } });
      await db.update({ _id: "prv_price" }, { $set: { lastPRVPriceDate: date.toISOString() } });

      console.log("-------------------\nPRV price reloaded!\n-------------------\n");
      return res.data.lastPRVPrice;
    } else return prv_price.prv_price;
  },
};
