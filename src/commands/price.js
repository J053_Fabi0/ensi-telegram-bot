/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const { handleErr } = require("../handleError");
const { getPRVPrice } = require("../PRVPrice");

const a = {};

a.setCommand = function (bot) {
  bot.command("price", async (ctx) => {
    try {
      const price = await getPRVPrice(ctx);
      await ctx.replyWithMarkdown(`The PRV is at \`${price}\` USD.\n\nPrice taken from incscan.io.`);
    } catch (err) {
      handleErr(err, ctx);
    }
  });
};

exports.price = a;
