/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Accounts = require("../classes/Accounts");
const { price } = require("./price");
const { db } = require("../../main");
const { askCreateAccount } = new Accounts(db);
const { handleErr } = require("../handleError");
const backup = require("./backup");

const a = {
  setCommands: function (bot) {
    price.setCommand(bot);
    bot.command("start", (ctx) => ctx.scene.enter("start").catch((err) => handleErr(err, ctx)));
    bot.command("addaccount", (ctx) => askCreateAccount(ctx));
    // backup.importBackupFile(bot);
    bot.hears(/^\/*exportjson/i, (ctx) => backup.sendBackupFile(ctx).catch((err) => handleErr(err, ctx)));

    bot.command("info", async (ctx) => {
      try {
        const publicKeys = await db.findOne({ _id: "publicKeys" });
        if (publicKeys) await ctx.reply(`Public keys: ${Object.keys(publicKeys).length - 1}`);
        else await ctx.reply("No publicKeys registered.");
      } catch (err) {
        handleErr(err, ctx);
      }
    });
  },
};

exports.commands = a;
