/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const { db } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { userNotInDB } = require("../messages");

const Nodes = require("../classes/Nodes");

const fs = require("fs");
const url = require("url");
const https = require("https");

function pDownload(url, dest_path) {
  const file = fs.createWriteStream(dest_path);
  return new Promise((resolve, reject) => {
    let responseSent = false; // flag to make sure that response is sent only once.
    https
      .get(url, (response) => {
        response.pipe(file);
        file.on("finish", () => {
          file.close(() => {
            if (responseSent) return;
            responseSent = true;
            resolve(file.path);
          });
        });
      })
      .on("error", (err) => {
        if (responseSent) return;
        responseSent = true;
        reject(err.toString());
      });
  });
}

function dbContainsHash(hash) {
  return new Promise((resolve, reject) =>
    db
      .findOne({ _id: "backupHashes" })
      .then((doc) => {
        if (doc && doc.hashes) resolve(doc.hashes.includes(hash));
        else resolve(false);
      })
      .catch((err) => reject(err))
  );
}

function getJSONFromURL(file_url, local_file_path) {
  return new Promise((resolve, reject) =>
    pDownload(file_url, local_file_path)
      .then((file_path) => {
        fs.readFile(file_path, "utf8", async (err, data) => {
          if (err) reject(err.toString());
          try {
            fs.unlinkSync(local_file_path);

            const dataParsed = JSON.parse(data);
            const validDoc = await dbContainsHash(hashCode(JSON.stringify(dataParsed, null, 4)));

            if (typeof validDoc == "boolean" && !validDoc)
              reject(
                "The JSON is not valid. Reasons could be that you have tampered with the data, because the hash of the file does not match any hash in the database."
              );
            else if (typeof validDoc != "boolean") reject(validDoc);

            resolve(dataParsed);
          } catch (err) {
            reject(err.toString());
          }
        });
      })
      .catch((err) => reject("Error while downloading: " + err))
  );
}

const hashCode = (s) =>
  s.split("").reduce((a, b) => {
    a = (a << 5) - a + b.charCodeAt(0);
    return a & a;
  }, 0);

let accounts = {};
module.exports = {
  sendBackupFile: async (ctx) => {
    try {
      const id = ctx.from.id.toString();

      const doc = await db.findOne({ _id: id });
      if (!doc) return await ctx.reply(userNotInDB);

      let backup = { user: doc };
      const nodesClass = new Nodes(db, id);

      for (let accountID of doc.accounts) {
        const account = await nodesClass.getAccountByID(accountID);
        backup[accountID] = account;
      }
      // Create json file with the documents
      fs.writeFile(`${process.env.PWD || "."}/${id}.json`, JSON.stringify(backup, null, 4), async (err) => {
        try {
          if (err) return handleErr(err, ctx);
          await ctx.replyWithChatAction("upload_document");

          // Send the file
          await ctx.replyWithDocument(
            {
              source: `${process.env.PWD || "."}/${id}.json`,
            },
            {
              caption: "This is all your data and accounts in JSON format.\n\n",
              // "Do not modify this file, or it won't be valid when trying to import it at a later time.\n\n" +
              // "To import this backup, simply send the file to me again.",
            }
          );

          // Add the hash of the document to the list of hashes. When importing a document, the has of that document needs to be in this array
          await db.update(
            { _id: "backupHashes" },
            { $addToSet: { hashes: hashCode(JSON.stringify(backup, null, 4)) } },
            { upsert: true }
          );

          // The file is deleted once it has been send
          fs.unlinkSync(`${process.env.PWD || "."}/${id}.json`);
        } catch (err) {
          handleErr(err, ctx);
        }
      });
    } catch (err) {
      handleErr(err, ctx);
    }
  },

  importBackupFile: (bot) => {
    let data;

    bot.on("document", async (ctx) => {
      const id = ctx.update.message.from.id.toString();
      const file = await bot.telegram
        .getFile(ctx.update.message.document.file_id)
        .catch((err) => handleErr(err, ctx));

      const nodesClass = new Nodes(db, id);

      // Check if it's a json file
      if (!new RegExp(/json$/).test(file.file_path)) return;

      const file_url = `https://api.telegram.org/file/bot${process.env.TOKEN}/${file.file_path}`;

      const file_name = url.parse(file_url).pathname.split("/").pop();
      const local_file_path = `${process.env.PWD || "."}/${file_name}`;

      getJSONFromURL(file_url, local_file_path)
        .then(async (res) => {
          data = res;

          try {
            for (const key of Object.keys(res)) {
              if (key == "user") continue;
              const accountName = await nodesClass.getAccountName(key);
              accounts[accountName] = key;
            }

            await ctx.replyWithMarkdown(
              "Importing a backup will override your data and the data of" +
                ` *"${Object.keys(accounts).join('"*, *"')}"*.` +
                "\nAre you sure you want to continue?",
              Markup.inlineKeyboard([
                [Markup.callbackButton("Yes, override my data with the backup", "confirmBackup")],
                [Markup.callbackButton("No", "cancelBackup")],
              ]).extra()
            );
          } catch (err) {
            handleErr(err, ctx);
          }
        })
        .catch((err) => handleErr(err, ctx));
    });

    bot.action("confirmBackup", async (ctx) => {
      try {
        db.update({ _id: ctx.from.id.toString() }, { $set: data.user }, { upsert: true }).catch((err) =>
          handleErr(err, ctx)
        );
        for (const key of Object.keys(accounts))
          db.update({ _id: accounts[key] }, { $set: data[accounts[key]] }, { upsert: true }).catch((err) =>
            handleErr(err, ctx)
          );

        await ctx.deleteMessage();
        await ctx.reply("All was succesfully updated. \u{1F44D}", menu);
      } catch (err) {
        handleErr(err, ctx);
      }
    });

    bot.action("cancelBackup", (ctx) => ctx.deleteMessage().catch((err) => handleErr(err, ctx)));
  },
};
