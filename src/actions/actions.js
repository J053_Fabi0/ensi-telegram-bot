/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

// const { db } = require("../../main");
const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { specialEmoji: SE } = require("../messages");

const a = {
  setActions: function (bot) {
    bot.action("create", (ctx) => ctx.scene.enter("createAccount").catch((err) => handleErr(err, ctx)));
    bot.action("import", (ctx) => ctx.scene.enter("importAccount").catch((err) => handleErr(err, ctx)));

    bot.action("addNode", (ctx) =>
      ctx
        .deleteMessage()
        .then(() => ctx.scene.enter("addNode").catch((err) => handleErr(err, ctx)))
        .catch((err) => handleErr(err, ctx))
    );

    bot.action("myNodes", (ctx) =>
      ctx
        .deleteMessage()
        .then(() => ctx.scene.enter("myNodes").catch((err) => handleErr(err, ctx)))
        .catch((err) => handleErr(err, ctx))
    );

    bot.action("addEarning", (ctx) =>
      ctx
        .deleteMessage()
        .then(() => ctx.scene.enter("addEarnings").catch((err) => handleErr(err, ctx)))
        .catch((err) => handleErr(err, ctx))
    );

    // Leave any scene and open keyboard menu
    bot.action(["leave", "cancel"], async (ctx) => {
      try {
        ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
        await ctx.reply(`${SE || "\u{1F3E0}"}`, menu);
        await ctx.scene.leave();
      } catch (err) {
        handleErr(err, ctx);
      }
    });

    // Default actions
    bot.action(/.*/, async (ctx) => {
      try {
        ctx.answerCbQuery().catch((err) => handleErr(err, ctx));
        await ctx.reply("Seems that you've tried to interact with an old message. Please try again from the begining.", menu);
        await ctx.scene.leave();
      } catch (err) {
        handleErr(err, ctx);
      }
    });
  },
};

exports.actions = a;
