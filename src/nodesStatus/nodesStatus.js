/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const axios = require("axios");
const Nodes = require("../classes/Nodes");
const { handleErr } = require("../handleError");
const { nodesStatusDB, db, bot } = require("../../main");
const { specialEmoji: SE } = require("../messages");

const nodesClass = new Nodes(db);

const MINUTE = 60_000;
const HOUR = MINUTE * 60;
const CHUNKS_OF_PUBKEYS_SEND = 10; // The nodes that queries to the fullnode at once. A big number may result in error 503 "Too busy"
const TIME_BETWEEN_CHECKS = MINUTE * 2; // Time in ms it takes after it has checked all the nodes status. Is like giving a short break to the fullnode
const COUNTS_BEFORE_DELETING = Math.ceil((HOUR * 5) / TIME_BETWEEN_CHECKS); // When a node has a role of -1, it sum up 1 to a counter, and if it's bigger than this constant, the public key is deleted.

const a = {};

async function getFullListRewards() {
  const id = Math.floor(Math.random() * 100);
  console.log("A request to get the full list of earnings has started. ID: " + id);

  const options = {
    method: "get",
    url: process.env.FULLNODE_DIRECTION,
    data: {
      id: 1,
      jsonrpc: "1.0",
      method: "listrewardamount",
      params: [],
    },
  };

  const res = await axios(options);
  console.log("The request to get the full list of earning has ended. ID: " + id);

  return res.data?.Result;
}

// getNodeRole
a.getNodeRole = async function (publicKey) {
  const options = {
    method: "get",
    url: process.env.FULLNODE_DIRECTION,
    data: {
      id: 1,
      jsonrpc: "1.0",
      method: "getincognitopublickeyrole",
      params: [publicKey],
    },
  };

  const res = await axios(options);
  return res.data?.Result?.Role;
};

// getNodeEarnings
a.getNodeEarnings = async function (publicKey) {
  const list = await getFullListRewards();
  const earnings = list?.[publicKey];
  if (!earnings) return 0;

  const { ["0000000000000000000000000000000000000000000000000000000000000004"]: prv } = earnings;
  const earningsPRV = prv ? prv / 1_000_000_000 : 0;

  return earningsPRV;
};

/**
 * Returns true if the node exists. This is done by asking the role; if its -1, it doesn’t exist.
 */
a.doesNodeExist = async function (publicKey) {
  const role = await a.getNodeRole(publicKey);
  return role >= 0;
};

/**
 * Saves the role in the nodesStatusDB database, and if earnings are given, it also saves them
 */
a.saveNodeRoleStatus = async function (publicKey, role, earnings = null) {
  await nodesStatusDB.update({ _id: publicKey }, { $set: { role: role, offline: 0 } }, { upsert: true });
  if (typeof earnings === "number")
    await nodesStatusDB.update({ _id: publicKey }, { $set: { earnings: earnings } }, { upsert: true });
};

a.getNodeRoleFromDB = async function (publicKey) {
  const oldStatus = await nodesStatusDB.findOne({ _id: publicKey });
  return oldStatus?.role || 0;
};

/**
 * Given the publicKey and the role, it checks if the role has chaged and act in consequence
 */
a.checkIfRoleIsNew = async function (publicKey, role) {
  // If the role is -1, increment the counter and maybe delete it.
  if (role === -1) {
    // Get the offline counter
    const data = await nodesStatusDB.findOne({ _id: publicKey });
    const { offline } = data || { offline: 0 };

    // If the counter is greater than the COUNTS_BEFORE_DELETING, delete the public key and return this function
    if (offline > COUNTS_BEFORE_DELETING) {
      const subscribers = await nodesClass.getSubscribersOfPublicKey(publicKey);
      for (const [userID, nodeName] of subscribers) {
        console.log(
          "--------------------------------\n" +
            `Message sended to ${userID} for node ${nodeName}, ${publicKey}, because node was deleted after some inactivity in role -1.` +
            "\n--------------------------------"
        );
        bot.telegram
          .sendMessage(
            userID,
            `Since *${nodeName}* has been offline in a while, I deleted its public key and I'm no longer listening to it.\n\n` +
              `You can always go to its settings and try adding a new public key, or the same, if the node goes back to normal at any time.`,
            {
              parse_mode: "Markdown",
            }
          )
          .catch((err) => handleErr(err, null, true));
      }
      nodesClass.deletePublicKey(publicKey).catch((err) => handleErr(err, null, true));
      nodesStatusDB.remove({ _id: publicKey }, {}).catch((err) => handleErr(err, null, true));
      return;
    } else {
      // Else, just increase the counter by 1
      nodesStatusDB
        .update({ _id: publicKey }, { $inc: { offline: 1 } }, { upsert: true })
        .catch((err) => handleErr(err, null, true));
    }
  }

  // Get the old status
  const oldStatus = await nodesStatusDB.findOne({ _id: publicKey });
  const oldRole = oldStatus?.role || 0;
  const oldEarnings = oldStatus?.earnings || 0;

  // Check if the latest status is different
  if (oldRole !== role) {
    // Save the new status in the database
    console.log("Do not turn off");
    await a.saveNodeRoleStatus(publicKey, role);
    processNewRoleInfo(publicKey, role, oldEarnings).catch((err) => handleErr(err, null, true));
  }
};

const util = require("util");
const sleep = util.promisify(setTimeout);
async function processNewRoleInfo(publicKey, role, oldEarnings) {
  // Get the IDs of the people subscribed to the node
  let { [publicKey]: accountsOfPublicKey } = await db.findOne({ _id: "publicKeys" });

  if (!accountsOfPublicKey) throw "This publicKey is not in the database: " + publicKey;

  let earnings = 0;
  if (role === 0 || role === 1) {
    // Waiting 30 seconds might solve the problem where sometimes the earning given by the fullnode is not
    // the more recent one
    await sleep(30 * 1000);
    earnings = await a.getNodeEarnings(publicKey);
  }

  const peopleNotified = [];
  // Because the same node can be in multiple accounts, this for needs to be done to cover each of them
  for (const [accountID, nodeName] of Object.entries(accountsOfPublicKey)) {
    const { accounts: subscribers } = await db.findOne({ _id: accountID });

    // If it's waiting to be selected, it's a good time to save the earnings it have,
    // so that when it finishes earning, the bot can tell if you haden't withdrawn the last earnings
    if (role === 1) await a.saveNodeRoleStatus(publicKey, role, earnings);

    // If it has stop earning, save the earnings in the user database
    if (role === 0 && earnings - oldEarnings !== 0)
      nodesClass
        .setNewEntry(earnings - oldEarnings, null, nodeName, accountID)
        .catch((err) => handleErr(err, null, true));

    // Choose the correct message to be sended
    let message = "";
    let buttons = [];
    switch (role) {
      case 0:
        message =
          `*${nodeName}* is back to waiting. ${SE}\n\nIt has \`${earnings - oldEarnings}\` PRV` +
          (oldEarnings !== 0
            ? `, and, since you hadn't withdrawn your earnings in a while, it has \`${earnings}\` PRV in total.`
            : " waiting for you.");
        break;
      case 1:
        message = `Hey! *${nodeName}* is waiting to be selected. ${SE}`;
        break;
      case 2:
        message = `${SE}\n*${nodeName}* is now validating some transactions.`;
        break;
      case -1:
        message =
          `Oh, no... *${nodeName}* seems to be off, because it has a role number of \`-1\`. Check it out.\n\n` +
          "I'll be monitoring it for a while, in case it's back to normal, and tell you if anything changes.\n" +
          `Its public key is set as \`${publicKey}\`.`;
        buttons.push([
          {
            text: `\u{1F504}\u{1F511} Change public key ${SE}`,
            callback_data: `changePublicKey_${accountID}_${nodeName}`,
          },
        ]);
        break;
      default:
        message =
          `I don't know what is this...\n\n*${nodeName}* has a role of \`${role}\`. ` +
          "Perhaps you could ask in the forum.";
    }

    // And send the message to every subscriber, ommiting the ones that have already been notified
    for (const subscriber of subscribers) {
      if (!peopleNotified.includes(subscriber)) {
        peopleNotified.push(subscriber);
        console.log(
          "--------------------------------\n" +
            `Message sended to ${subscriber} for node ${nodeName}, ${publicKey}, and role ${role}:\n${message}` +
            "\n--------------------------------"
        );
        bot.telegram
          .sendMessage(subscriber, message, {
            reply_markup: {
              inline_keyboard: buttons,
            },
            parse_mode: "Markdown",
          })
          .catch((err) => handleErr(err, null, true));
      }
    }
  }
  console.log("Is safe to turn the bot down");
}

// This function returns a promise that queries the fullnode CHUNKS_OF_PUBKEYS_SEND times at once.
// So, if CHUNKS_OF_PUBKEYS_SEND is 5, then given the publicKeys available and the index, it sends
// 5 queries at the same time to the fullnode
function callNextChunk(n, publicKeys) {
  return new Promise((resolve) => {
    for (let i = n; i < n + CHUNKS_OF_PUBKEYS_SEND; i++) {
      if (i >= publicKeys.length) break;

      const publicKey = publicKeys[i];
      a.getNodeRole(publicKey)
        .then(async (role) => {
          // Call the function that will process this new information
          await a.checkIfRoleIsNew(publicKey, role).catch((err) => handleErr(err, null, true));
        })
        .catch((err) => handleErr(err, null, true))
        .finally(() => {
          // It only resolves once (n + 1) is multiple of the CHUNKS_OF_PUBKEYS_SEND, or if it's equal to the length
          // of the publicKeys array
          if (++n === publicKeys.length || n % CHUNKS_OF_PUBKEYS_SEND === 0) resolve();
        });
    }
  });
}

let checkCount = 1;
// main
a.main = function () {
  setTimeout(() => {
    console.log("Check " + checkCount++);
    nodesClass
      .getPublicKeys()
      .then(async (publicKeys) => {
        const maxNumberOfRepetitions =
          Math.ceil(publicKeys.length / CHUNKS_OF_PUBKEYS_SEND) * CHUNKS_OF_PUBKEYS_SEND;

        for (let n = 0; n < maxNumberOfRepetitions; n += CHUNKS_OF_PUBKEYS_SEND)
          await callNextChunk(n, publicKeys);
      })
      .catch((err) => handleErr(err, null, true))
      .finally(() => a.main());
  }, TIME_BETWEEN_CHECKS);
};

exports.nodesStatus = a;
