/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const { bot } = require("../main");

const handleErr = async function (err, ctx, callOnlyTheAdmin = false) {
  const id = Math.floor(Math.random() * 10000);
  console.log(`Handled error #${id}: `, err);

  const messageToUser =
    "There was an error" +
    `. Could you please redirect this message to ${process.env.ADMIN_ALIAS}?` +
    `\n\`\`\`\nError ID: ${id}\`\`\``;
  const messageToAmin =
    "There was an error" +
    `, Mr ${process.env.ADMIN_ALIAS || "Admin"}. ID: ${id}` +
    `\n\`\`\`\n${typeof err === "object" && err.Message ? JSON.stringify(err) : err}\`\`\``;

  if (
    err?.response?.description ==
      "Bad Request: message is not modified: specified new message content and reply markup are exactly the same as a current content and reply markup of the message" ||
    err?.response?.description == "Bad Request: query is too old and response timeout expired or query ID is invalid" ||
    err?.response?.description == "Bad Request: chat not found"
  )
    return;

  try {
    if (!callOnlyTheAdmin) await ctx.replyWithMarkdown(messageToUser);
  } catch (err) {
    console.log("There was another error: ", err);
  } finally {
    if (process.env.ADMIN_ID)
      bot.telegram
        .sendMessage(process.env.ADMIN_ID, messageToAmin, { parse_mode: "Markdown" })
        .catch((err) =>
          console.log("There was an error contacting the admin. Please, check the ADMIN_ID environmental variable: ", err)
        );
    else console.log("There was an error contacting the admin. Please, set the ADMIN_ID environmental variable.");
  }
};

exports.handleErr = handleErr;
