/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");
const DateFormat = require("../DateFormat");
const { noNodes, userNotInDB, specialEmoji: SE } = require("../messages");

class Nodes {
  constructor(db, accountID = null) {
    this.db = db;
    this.accountID = accountID;
    this.accountName;
    this.nodeName;
    this.nodePublicKey;
    this.accounts;
    this.nodes;
    this.total;
    this.lastEarningDate;
    this.oldestEarningDate;
  }

  async getAccountByID(accountID = null) {
    this.accountID = accountID || this.accountID;

    const doc = await this.db.findOne({ _id: this.accountID });

    if (!doc) return;

    this.nodes = doc.nodes;
    this.accountName = doc.name;
    this.accounts = doc.accounts;

    return doc;
  }

  async getNodes(accountID = this.accountID) {
    await this.getAccountByID(accountID);

    return this.nodes;
  }

  /**
   * Returns an array with the names of all the nodes in the account
   */
  async getNodesNames(accountID = this.accountID) {
    const nodes = await this.getNodes(accountID);
    return nodes ? Object.keys(nodes) : [];
  }

  async getAccountAccounts(accountID = this.accountID) {
    await this.getAccountByID(accountID);
    return this.accounts;
  }

  async getAccountName(accountID = this.accountID) {
    await this.getNodes(accountID);
    return this.accountName;
  }

  async getInlineKeyboardNodes(
    accountID = this.accountID,
    showBackButton = false,
    cancelBtnIfNoShowBack = false,
    showSettingsButton = false
  ) {
    const nodes = await this.getNodes(accountID);
    const nodesNames = Object.keys(nodes);
    const buttons = [];
    for (const name of nodesNames) {
      let [emoji] = await this.getEmojiOfNodeName(name, accountID);

      buttons.push([Markup.callbackButton(`${emoji} ${name} ${SE}`, `select_${name}`)]);
    }

    const settingsButton = Markup.callbackButton(`\u2699 Account settings`, `settings_${accountID}`);
    const backButton = Markup.callbackButton(`\u2B05 Go back`, "backAccount");
    const cancelButton = Markup.callbackButton(`\u{274C} Cancel`, "cancel");

    if (showBackButton) {
      buttons.push(showSettingsButton ? [backButton, settingsButton] : [backButton]);
    } else if (cancelBtnIfNoShowBack) {
      buttons.push([cancelButton]);
    } else if (showSettingsButton) {
      buttons.push([settingsButton]);
    }

    return Markup.inlineKeyboard(buttons).extra();
  }

  /**
   * Returns an array with two elements: [emoji, descriptionOfEmoji], both strings.
   */
  async getEmojiOfNodeName(nodeName, accountID = this.accountID) {
    const { nodesStatus } = require("../nodesStatus/nodesStatus");
    const publicKey = await this.getPublicKeyOfNode(nodeName, accountID);

    if (publicKey) {
      const role = await nodesStatus.getNodeRoleFromDB(publicKey);
      switch (role) {
        case -1:
          return ["⚪️", "Offline"];
        case 0:
          return ["🟢", "Waiting to be selected"];
        case 1:
          return ["🟡", "Pending"];
        case 2:
          return ["🔵", "Earning"];
      }
    } else return ["\u{1F511} \u{1F937}\u200D\u2642\uFE0F", "No public key given"];
  }

  /**
   * Return an array with all the keys from the earnings of the node
   * @return array
   */
  async getEarningsKeys(accountID = this.accountID, nodeName = this.nodeName) {
    const nodes = await this.getNodes(accountID);
    if (!nodes[nodeName].earnings) return [];
    return Object.keys(nodes[nodeName].earnings);
  }

  /**
   * Returns all the earnings from a node.
   * @return Object
   */
  async getEarningsFromNode(accountID = this.accountID, nodeName = this.nodeName) {
    const nodes = await this.getNodes(accountID);
    return nodes[nodeName].earnings;
  }

  /**
   * Get how many times all the nodes had earned
   */
  async getHowManyTimesTheyEarned(date = null, accountID = this.accountID) {
    return await this.getTotalEarningsForAll(date, accountID, true);
  }

  /**
   * Get how many times a node has earned
   */
  async getHowManyTimesItEarned(date = null, nodeName = this.nodeName, accountID = this.accountID) {
    return await this.getTotalEarningsForNode(date, nodeName, accountID, true);
  }

  async _getOldestLastEarningDateNode(lastDate = true, accountID = this.accountID, nodeName = this.nodeName) {
    const nodes = await this.getNodes(accountID);
    const lastOrOldest = lastDate ? "lastEarningDate" : "oldestEarningDate";

    if (nodes[nodeName][lastOrOldest]) {
      return nodes[nodeName][lastOrOldest];
    } else {
      let earningKeys = await this.getEarningsKeys(accountID, nodeName);
      if (earningKeys.length === 0) return 0;

      const date = lastDate ? DateFormat.getGreaterDate(earningKeys) : DateFormat.getOldestDate(earningKeys);

      nodes[nodeName][lastOrOldest] = date;
      await this.db.update({ _id: accountID }, { $set: { nodes: nodes } });

      return date;
    }
  }

  /**
   * Returns the last earning date of the node, in the format YYYY-MM-DD.
   * @returns string
   */
  async getLastEarningDateNode(accountID = this.accountID, nodeName = this.nodeName) {
    return await this._getOldestLastEarningDateNode(true, accountID, nodeName);
  }

  /**
   * Returns an array of arrays [name, date] sorted with the 0 (no registered earnings) at the bottom and the ones with an oldest earning date at the top.
   */
  async getLastEarningDateForAll(accountID = this.accountID) {
    const nodes = await this.getNodesNames(accountID);

    let arr = [];
    for (let i = 0; i < nodes.length; i++) {
      const date = await this.getLastEarningDateNode(accountID, nodes[i]);
      // The 3rt element is just a numeric representation of the date, used to sort them
      arr.push([nodes[i], date, Number(date.toString().replace(/-/g, ""))]);
    }

    // It sorts the array, placing the ones with an oldest earning date at the top
    arr.sort((a, b) => a[2] - b[2]);

    // Now I remove the 3rd element
    arr = arr.map((a) => [a[0], a[1]]);

    // And move the nodes with no data to the bottom
    const noDataNodes = arr.filter((a) => a[1] === 0);
    const nodesWithData = arr.filter((a) => a[1] !== 0);

    return [...nodesWithData, ...noDataNodes];
  }

  /**
   * Returns the oldest earning date of the node, in the format YYYY-MM-DD.
   * @returns string
   */
  async getOldestEarningDateNode(accountID = this.accountID, nodeName = this.nodeName) {
    return await this._getOldestLastEarningDateNode(false, accountID, nodeName);
  }

  /**
   * Returns the days since the node hasn't earned. If you use it in conjunction with getLastEarningDateNode, is recommended to pass the lastEarningDate.
   */
  async getDaysSinceLastEarningNode(lastEarningDate = "", accountID = this.accountID, nodeName = this.nodeName) {
    if (!lastEarningDate) lastEarningDate = await this.getLastEarningDateNode(accountID, nodeName);
    if (lastEarningDate == 0) return 0;

    const today = DateFormat.getOnlyDate();

    return DateFormat.getDaysFromDateToDate(today, lastEarningDate);
  }

  async getTotalEarningsForNode(date = null, nodeName = this.nodeName, accountID = this.accountID, getOnlyCount = false) {
    // earningKeys is a list of all the days it has earned
    const earningKeys = await this.getEarningsKeys(accountID, nodeName);
    if (earningKeys.length === 0) return 0;

    // earnings is the object, containing all the earnings for a node
    const earnings = await this.getEarningsFromNode(accountID, nodeName);

    const [year, month] = date ? date.split("-") : [null, null];

    let count = 0;
    if (year && month) {
      // This regex will filter the total earnings only for a specific year and a month
      const regex = new RegExp(`${year}-${month}`);

      let totalThisMonth = 0;
      // This filters the keys using the nodes
      const earningKeysThisMonth = earningKeys.filter((day) => regex.test(day));
      if (earningKeysThisMonth.length === 0) return 0;

      for (const day of earningKeysThisMonth)
        for (const earning of earnings[day]) {
          totalThisMonth += earning;
          count++;
        }

      if (getOnlyCount) return count;
      else return Number(totalThisMonth.toFixed(4));
    } else {
      if (earningKeys.length === 0) return 0;

      let total = 0;
      for (const day of earningKeys)
        for (const earning of earnings[day]) {
          total += earning;
          count++;
        }

      if (getOnlyCount) return count;
      else return Number(total.toFixed(4));
    }
  }

  async getTotalEarningsForAll(date = null, accountID = this.accountID, getOnlyCount = false) {
    const nodes = await this.getNodesNames(accountID);

    let total = 0;
    for (const nodeName of nodes) {
      total += await this.getTotalEarningsForNode(date, nodeName, accountID, getOnlyCount);
    }

    return Number(total.toFixed(4));
  }

  async getAverageEarningsForNode(date = null, nodeName = this.nodeName, accountID = this.accountID) {
    // earningKeys is a list of all the days it has earned
    const earningKeys = await this.getEarningsKeys(accountID, nodeName);
    if (earningKeys.length === 0) return 0;
    // earnings is the object, containing all the earnings for a node
    const earnings = await this.getEarningsFromNode(accountID, nodeName);

    const [year, month] = date ? date.split("-") : [null, null];

    if (year && month) {
      // This regex will filter the total earnings only for a specific year and a month
      const regex = new RegExp(`${year}-${month}`);

      // This filters the keys using the nodes
      const earningKeysThisMonth = earningKeys.filter((day) => regex.test(day));
      if (earningKeysThisMonth.length === 0) return 0;

      let numberOfEarnings = 0;
      let averageThisMonth = 0;
      for (const day of earningKeysThisMonth) {
        for (const earning of earnings[day]) {
          averageThisMonth += earning;
          numberOfEarnings++;
        }
      }

      return Number((averageThisMonth / numberOfEarnings).toFixed(4));
    } else {
      if (earningKeys.length === 0) return 0;

      let numberOfEarnings = 0;
      let average = 0;
      for (const day of earningKeys) {
        for (const earning of earnings[day]) {
          average += earning;
          numberOfEarnings++;
        }
      }

      return Number((average / numberOfEarnings).toFixed(4));
    }
  }

  async getAverageEarningsForAll(date = null, accountID = this.accountID) {
    const nodes = await this.getNodesNames(accountID);
    if (nodes.length === 0) return 0;

    let total = 0;
    let numberOfEarnings = 0;
    for (const node of nodes) {
      const averageEarning = await this.getAverageEarningsForNode(date, node, accountID);
      if (averageEarning) numberOfEarnings++;
      total += averageEarning;
    }

    if (numberOfEarnings === 0) return 0;
    return Number((total / numberOfEarnings).toFixed(4));
  }

  /**
   * Set a new earning entry for a node inside an account
   */
  async setNewEntry(prvsEarned, date = null, nodeName = this.nodeName, accountID = this.accountID) {
    if (!accountID) return "No accountID provided";

    prvsEarned = Number(prvsEarned);
    const nodes = await this.getNodes(accountID);

    date = date ? date : DateFormat.getOnlyDate();

    const lastEarningDate = await this.getLastEarningDateNode(accountID, nodeName);
    const oldestEarningDate = await this.getOldestEarningDateNode(accountID, nodeName);

    nodes[nodeName].lastEarningDate = lastEarningDate === 0 ? date : DateFormat.getGreaterDate([lastEarningDate, date]);
    nodes[nodeName].oldestEarningDate = oldestEarningDate === 0 ? date : DateFormat.getOldestDate([oldestEarningDate, date]);

    let earnings = nodes[nodeName].earnings[date];

    if (earnings) nodes[nodeName].earnings[date].push(prvsEarned);
    else nodes[nodeName].earnings[date] = [prvsEarned];

    nodes[nodeName].total = nodes[nodeName].total + prvsEarned;

    await this.db.update({ _id: accountID }, { $set: { nodes: nodes } });
  }

  async checkIfHasNodes(ctx, editMessage, callback = null, accountID = this.accountID) {
    const id = ctx.from.id.toString();

    const nodesNames = await this.getNodesNames(accountID);

    const Accounts = require("./Accounts");
    const accountsClass = new Accounts(this.db, id);
    const accounts = await accountsClass.getUserAccounts(id);
    const numberOfAccounts = Object.keys(accounts).length;

    // If you have no nodes
    if (nodesNames.length == 0) {
      const keyboard =
        numberOfAccounts > 1
          ? Markup.inlineKeyboard([
              [
                Markup.callbackButton(`\u2B05 Go back ${SE}`, "backAccount"),
                Markup.callbackButton(`➕ Add node ${SE}`, "addNode"),
              ],
              [Markup.callbackButton(`\u2699 Account settings ${SE}`, `settings_${accountID}`)],
            ]).extra()
          : Markup.inlineKeyboard([
              Markup.callbackButton(`➕ Add node ${SE}`, "addNode"),
              Markup.callbackButton(`\u2699 Account settings ${SE}`, `settings_${accountID}`),
            ]).extra();

      if (editMessage) await ctx.editMessageText(noNodes, keyboard);
      else await ctx.reply(noNodes, keyboard);

      if (numberOfAccounts == 1) await ctx.scene.leave();
    }

    callback?.();
  }

  async showNodes(ctx, editMessage, accountID = this.accountID, customMessage = "", cancelBtn, settingsButton) {
    const id = ctx.from.id.toString();

    const nodes = await this.getNodes(accountID);
    const nodesNames = await this.getNodesNames(accountID);

    const Accounts = require("./Accounts");
    const accountsClass = new Accounts(this.db, id);
    const accounts = await accountsClass.getUserAccounts(id);
    const numberOfAccounts = Object.keys(accounts).length;

    // This means you are not in the DB at all!
    if (!nodes) {
      if (editMessage) await ctx.editMessageText(userNotInDB);
      else await ctx.reply(userNotInDB);
      await ctx.scene.leave();
      return;
    }

    let buttons = [[Markup.callbackButton(`➕ Add node ${SE}`, "addNode")]];
    if (numberOfAccounts > 1) buttons[0].unshift(Markup.callbackButton(`\u2B05 Go back ${SE}`, "backAccount"));
    if (settingsButton) buttons.push([Markup.callbackButton(`\u2699 Account settings ${SE}`, `settings_${accountID}`)]);

    // If you have no nodes
    if (nodesNames.length == 0) {
      const keyboard = Markup.inlineKeyboard(buttons).extra();

      if (editMessage) await ctx.editMessageText(noNodes, keyboard);
      else await ctx.reply(noNodes, keyboard);

      return;
    }

    const keyboard = await this.getInlineKeyboardNodes(accountID, numberOfAccounts > 1, cancelBtn, settingsButton);
    const message =
      customMessage ||
      `${nodesNames.length == 1 ? "This one is, for now, your only" : "These are your " + nodesNames.length}` +
        ` node${nodesNames.length == 1 ? "" : "s"}.` +
        `\nClick on ${nodesNames.length == 1 ? "it" : "them"} to see more options and details.`;

    if (editMessage) await ctx.editMessageText(message, keyboard);
    else await ctx.reply(message, keyboard);
  }

  async isNameInUse(nodeName, accountID = this.accountID) {
    if (!accountID) throw "No accountID provided";

    const doc = await this.db.findOne({ _id: accountID });

    return nodeName in doc.nodes;
  }

  async addNewNode(ctx, nodeName, editMessage, accountID = this.accountID) {
    if (!accountID) throw "No accountID provided.";

    const doc = await this.db.findOne({ _id: accountID });
    let message, buttons;

    if (!(nodeName in doc.nodes)) {
      doc.nodes[nodeName] = {
        earnings: {},
        total: 0,
        added: DateFormat.getOnlyDate(),
      };

      this.db.update({ _id: accountID }, { $set: { nodes: doc.nodes } });

      message = `I've added *"${nodeName}"* to your list of nodes.`;
      buttons = Extra.markup(
        Markup.inlineKeyboard([
          [
            Markup.callbackButton(`➕ Add another node ${SE}`, "addNode"),
            Markup.callbackButton(`\u{1F5C3} See my nodes ${SE}`, "myNodes"),
          ],
          [Markup.callbackButton(`\u{1F3E0} Open keyboard menu ${SE}`, "leave")],
        ])
      ).markdown();
    } else {
      message = `*Great!* Now "${nodeName}" has a public key, so I will automatically detect its earnings from now on.`;
      buttons = Extra.markup(
        Markup.inlineKeyboard([
          [Markup.callbackButton(`\u{1F5C3} See my nodes ${SE}`, "myNodes")],
          [Markup.callbackButton(`\u{1F3E0} Open keyboard menu ${SE}`, "leave")],
        ])
      ).markdown();
    }

    if (editMessage) await ctx.editMessageText(message, buttons);
    else await ctx.reply(message, buttons);
  }

  async addPublicKeyToNode(nodeName, publicKey, accountID = this.accountID) {
    const oldPublicKey = await this.getPublicKeyOfNode(nodeName, accountID);

    // If there is an oldPublicKey, that means that is trying to replace the publicKey of that oldPublicKey
    if (oldPublicKey && oldPublicKey !== publicKey) {
      const data = await this.db.findOne({ _id: "publicKeys" });

      // If this public key has only one account, delete it
      if (Object.keys(data[oldPublicKey]).length === 1) await this.deletePublicKey(oldPublicKey);
      // Else, just delete the account from the oldPublicKey, leaving the others as they are
      else await this.db.update({ _id: "publicKeys" }, { $unset: { [`${oldPublicKey}.${accountID}`]: true } });
    }

    await this.db.update({ _id: "publicKeys" }, { $set: { [`${publicKey}.${accountID}`]: nodeName } }, { upsert: true });
  }

  /**
   * Deletes the public key completely from database
   */
  async deletePublicKey(publicKey) {
    await this.db.update({ _id: "publicKeys" }, { $unset: { [publicKey]: true } });
  }

  /**
   * Returns either false or the name of the node that has the public key in the account
   */
  async isPublicKeyUsedInAccount(publicKey, accountID = this.accountID) {
    if (!publicKey) throw "No public key given";

    const publicKeys = await this.db.findOne({ _id: "publicKeys" });

    return publicKeys?.[publicKey]?.[accountID] || false;
  }

  /**
   * Returns an array with all the public keys
   */
  async getPublicKeys() {
    const publicKeys = await this.db.findOne({ _id: "publicKeys" });
    if (!publicKeys) return [];

    delete publicKeys._id;
    return Object.keys(publicKeys);
  }

  /**
   * Returns the publicKey of the matching nodeName and accountID, and an empty string if nothing is found.
   */
  async getPublicKeyOfNode(nodeName = this.nodeName, accountID = this.accountID) {
    if (!accountID) throw "No account id given.";
    if (!nodeName) throw "No node name given.";

    const publicKeys = await this.db.findOne({ _id: "publicKeys" });
    if (!publicKeys) return "";

    delete publicKeys._id;
    const keys = Object.keys(publicKeys);

    for (const key of keys) if (publicKeys[key]?.[accountID] === nodeName) return key;

    return "";
  }

  /**
   * Returns an array with all the keys an account have. The array is empty in case no key is found
   */
  async getPublicKeysOfAccount(accountID = this.accountID) {
    if (!accountID) throw "No account id given.";

    const publicKeys = await this.db.findOne({ _id: "publicKeys" });
    if (!publicKeys) return [];

    delete publicKeys._id;
    const keys = Object.keys(publicKeys);

    const foundKeys = [];

    for (const key of keys) if (accountID in publicKeys[key]) foundKeys.push(key);

    return foundKeys;
  }

  /**
   * Returns an array with all the accounts where the publicKey is.
   */
  async getAccountsOfPublicKey(publicKey) {
    const publicKeys = await this.db.findOne({ _id: "publicKeys" });
    if (!publicKeys) return [];
    if (!publicKeys[publicKey]) return [];

    const accounts = publicKeys?.[publicKey];

    return accounts ? Object.keys(accounts) : [];
  }

  /**
   * Returns an array of arrays with all the users ID and node names that are subscribed to the public key. [[userID, nodeName]]
   */
  async getSubscribersOfPublicKey(publicKey) {
    let { [publicKey]: accountsOfPublicKey } = await this.db.findOne({ _id: "publicKeys" });
    if (!accountsOfPublicKey) throw "This publicKey is not in the database: " + publicKey;

    const finalSubscribers = [];
    // Because the same node can be in multiple accounts, this for needs to be done to cover each of them
    for (const [accountID, nodeName] of Object.entries(accountsOfPublicKey)) {
      const { accounts: subscribers } = await this.db.findOne({ _id: accountID });
      for (const subscriber of subscribers)
        if (finalSubscribers.findIndex(([a]) => a === subscriber) === -1) finalSubscribers.push([subscriber, nodeName]);
    }

    return finalSubscribers;
  }
}

module.exports = Nodes;
