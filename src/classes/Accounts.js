/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const Nodes = require("./Nodes");

const { menu } = require("../keyboards");
const { handleErr } = require("../handleError");
const { isUserInDB } = require("../isUserInDB");
const { userNotInDB, noAccount, whatIsAnAccount, specialEmoji: SE } = require("../messages");

class Accounts {
  constructor(db, id = null) {
    this.db = db;
    this.id = id;
    this.accounts;
    this.userName;
    this.showInUSD;
    this.settings;
    this.sortMethod;
  }

  setAccountID(accountID) {
    return (this.id = accountID);
  }

  loadUserData(id = null) {
    return new Promise((resolve, reject) => {
      this.id = id || this.id;

      if (!this.id) return reject("No id provided");
      if (!this.db) return reject("No database provided to the constructor.");

      this.db
        .findOne({ _id: this.id })
        .then((doc) => {
          if (!doc) return resolve();
          this.accounts = doc.accounts || [];
          this.userName = doc.name || "No name";
          this.showInUSD = doc.showInUSD || false;
          this.settings = doc.settings || {};
          // Possible methods = lastEarningDate, earningsOfTheMonth, totalEarnings
          this.sortMethod = doc.sortMethod || "lastEarningDate";
          resolve(doc);
        })
        .catch((err) => reject(err));
    });
  }

  async getShowInUSD(id = this.id) {
    await this.loadUserData(id);
    return this.showInUSD;
  }

  async setShowInUSD(showInUSD, id = this.id) {
    await this.db.update({ _id: id }, { $set: { showInUSD: showInUSD } }, { upsert: true });
    this.showInUSD = showInUSD;
    return showInUSD;
  }

  async getSortMethod(id = this.id) {
    await this.loadUserData(id);
    return this.sortMethod;
  }

  async setSortMethod(sortMethod, id = this.id) {
    await this.db.update({ _id: id }, { $set: { sortMethod: sortMethod } }, { upsert: true });
    this.sortMethod = sortMethod;
    return sortMethod;
  }

  async getSettings(id = this.id) {
    await this.loadUserData(id);
    return this.settings;
  }

  async setSettings(settings, id = this.id) {
    await this.db.update({ _id: id }, { $set: { settings: settings } }, { upsert: true });
    this.settings = settings;
    return settings;
  }

  async getUserName(id = this.id) {
    await this.loadUserData(id);
    return this.userName;
  }

  /**
   * Returns the array with the ID's of the accounts
   */
  async getUserAccounts(id = this.id) {
    await this.loadUserData(id);
    return this.accounts;
  }

  /**
   * This returns an object with the id of each account as name, and the name of that node as value.
   */
  async getUserAccountsNames(id = this.id) {
    if (!this.db) throw "No database provided to the constructor.";

    const accounts = await this.getUserAccounts(id);
    const nodesClass = new Nodes(this.db);

    const accountsNames = {};
    for (const account of accounts) {
      const accountName = await nodesClass.getAccountName(account);
      accountsNames[account] = accountName;
    }

    return accountsNames;
  }

  async getInlineKeyboardAccounts(cancelBtn = false, id = this.id) {
    const accounts = await this.getUserAccountsNames(id);
    const buttons = Object.keys(accounts).map((account) => [
      Markup.callbackButton(`${SE} ${accounts[account]} ${SE}`, `account_${account}`),
    ]);

    if (cancelBtn) buttons.push([Markup.callbackButton(`\u{274C} Cancel ${SE}`, "cancel")]);
    return Markup.inlineKeyboard(buttons).extra();
  }

  /**
   * Given ctx, it reply with options to setup a new account or import one.
   */
  async askCreateAccount(ctx) {
    try {
      const id = ctx.from.id.toString();
      const isInDB = await isUserInDB(id);
      if (!isInDB) {
        await ctx.reply(userNotInDB);
        return;
      }

      await ctx.replyWithMarkdown(
        "You can either create a new account for a set of nodes, or import an existing one if a friend gives you the key." +
          whatIsAnAccount,
        Markup.inlineKeyboard([
          [Markup.callbackButton(`\u2795 Create new account ${SE}`, "create")],
          [Markup.callbackButton(`\u2B07 Import account ${SE}`, "import")],
        ]).extra()
      );
    } catch (err) {
      handleErr(err, ctx);
    }
  }

  /**
   * Given ctx, it show a message with the accounts with an inlineKeyboard. If it only have one account, returns.
   */
  async showAccounts(
    ctx,
    editMessage,
    cancelBtn = false,
    showNodesIfSingleAccount = true,
    customMessageIfSingleAccount = "",
    cancelBtnIfSingleAccount = false,
    settingsButtonIfSingleAccount = false
  ) {
    const id = ctx.from.id.toString();
    const isInDB = await isUserInDB(id);
    if (!isInDB) {
      if (editMessage) await ctx.editMessageText(userNotInDB);
      else await ctx.reply(userNotInDB);
      await ctx.scene.leave();
      return;
    }

    const accounts = await this.getUserAccountsNames(id);
    if (Object.keys(accounts).length === 0) {
      if (editMessage) {
        await ctx.deleteMessage();
        await ctx.reply(noAccount, menu);
      } else {
        await ctx.reply(noAccount, menu);
      }
      await ctx.scene.leave();
      return;
    }

    const numberOfAccounts = Object.keys(accounts).length;

    // If you only have one account, there is no point of showing a menu of the accounts you have
    // It's better to go straight to showing the nodes for that account
    const nodesClass = new Nodes(this.db, Object.keys(accounts)[0]);
    if (numberOfAccounts == 1) {
      if (showNodesIfSingleAccount)
        await nodesClass.showNodes(
          ctx,
          editMessage,
          accounts[0],
          customMessageIfSingleAccount,
          cancelBtnIfSingleAccount,
          settingsButtonIfSingleAccount
        );
      return;
    }

    const keyboard = await this.getInlineKeyboardAccounts(cancelBtn, id);
    const message = "Choose one account";

    if (editMessage) await ctx.editMessageText(message, keyboard);
    else await ctx.reply(message, keyboard);
  }

  async deleteAccount(accountID, id = this.id) {
    const account = await this.db.findOne({ _id: accountID });

    if (account.accounts.length == 1) {
      // If you are the only person using that account, then it needs to be removed
      await this.db.remove({ _id: accountID });

      // Also delete the public keys it may have
      const nodesClass = new Nodes(this.db, accountID);
      const keysItHave = await nodesClass.getPublicKeysOfAccount();

      if (keysItHave.length > 0) {
        const doc = await this.db.findOne({ _id: "publicKeys" });

        for (const publicKey of keysItHave) {
          delete doc[publicKey][accountID];
          if (Object.keys(doc[publicKey]).length === 0)
            await this.db.update({ _id: "publicKeys" }, { $unset: { [publicKey]: true } });
          else await this.db.update({ _id: "publicKeys" }, { $set: { [publicKey]: doc[publicKey] } });
        }
      }
    } else {
      const newAccounts = account.accounts;
      newAccounts.splice(newAccounts.indexOf(id), 1);

      await this.db.update({ _id: accountID }, { $set: { accounts: newAccounts } });
    }

    const user = await this.db.findOne({ _id: id.toString() });

    const newAccounts = user.accounts;
    newAccounts.splice(newAccounts.indexOf(accountID), 1);

    await this.db.update({ _id: id.toString() }, { $set: { accounts: newAccounts } });
  }
}

module.exports = Accounts;
