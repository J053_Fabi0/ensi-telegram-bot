/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const Markup = require("telegraf/markup");
const { specialEmoji: SE } = require("./messages");

const menu = Markup.keyboard([
  [`\u{1F4B8} Add earning entry ${SE}`],
  [`\u{1F4CA} Statistics ${SE}`],
  [`\u{1F5C3} My nodes ${SE}`],
  [`➕ Add node ${SE}`, `\u2699 Settings ${SE}`],
])
  .oneTime()
  .resize()
  .extra();

exports.menu = menu;
