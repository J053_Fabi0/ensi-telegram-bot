/*
  This file is part of Earn Node Statistics Incognito Telegram Bot - Ensi Telegram Bot.

  Ensi Telegram Bot is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Ensi Telegram Bot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Node Earn Statistics Telegram Bot.  If not, see <https://www.gnu.org/licenses/>.
*/

const dotenv = require("dotenv");
dotenv.config();

const Datastore = require("nedb-promises");

const db = Datastore.create({ filename: "./db/database.db", autoload: true });
db.persistence.setAutocompactionInterval(86_400_000); // Compacts database every day
exports.db = db;

const nodesStatusDB = Datastore.create({ filename: "./db/nodesStatus.db", autoload: true });
nodesStatusDB.persistence.setAutocompactionInterval(86_400_000);
exports.nodesStatusDB = nodesStatusDB;

const Telegraf = require("telegraf");
const bot = new Telegraf(process.env.TOKEN);
exports.bot = bot;

// The minutes before any scene automatically leaves to prevent bugs when they are keep unused for long periods of time.
exports.timeOut = 1000 * 60 * 20;

const Stage = require("telegraf/stage");
const session = require("telegraf/session");
const { menu } = require("./src/keyboards");
const { handleErr } = require("./src/handleError");
const { specialEmoji: SE } = require("./src/messages");
const { nodesStatus } = require("./src/nodesStatus/nodesStatus");

bot.catch((err, ctx) => handleErr(err, ctx));

// Add all scenes to the stage
const scenes = require("./src/scenes/Scenes");
const stage = new Stage([...Object.values(scenes)]);

bot.use(session()).catch((err) => console.log(err));
bot.use(stage.middleware()).catch((err) => console.log(err));

const { commands } = require("./src/commands/commands");
commands.setCommands(bot);

const { actions } = require("./src/actions/actions");
actions.setActions(bot);

bot.hears(["\u2699 Settings", `\u2699 Settings ${SE}`], (ctx) =>
  ctx.scene.enter("settings").catch((err) => handleErr(err, ctx))
);

bot.hears(["➕ Add node", `➕ Add node ${SE}`], (ctx) =>
  ctx.scene.enter("addNode").catch((err) => handleErr(err, ctx))
);

bot.hears(["\u{1F5C3} My nodes", `\u{1F5C3} My nodes ${SE}`], (ctx) =>
  ctx.scene.enter("myNodes").catch((err) => handleErr(err, ctx))
);

bot.hears(["\u{1F4B8} Add earning entry", `\u{1F4B8} Add earning entry ${SE}`], (ctx) =>
  ctx.scene.enter("addEarnings").catch((err) => handleErr(err, ctx))
);

bot.hears(["\u{1F4CA} Statistics", `\u{1F4CA} Statistics ${SE}`], (ctx) =>
  ctx.scene.enter("showStatistics").catch((err) => handleErr(err, ctx))
);

bot.on("message", (ctx) =>
  ctx
    .reply(
      "Please, use one of the options from the custom keyboard or write a slash (/) to see the available commands.",
      menu
    )
    .catch((err) => handleErr(err, ctx))
);

bot
  .launch()
  .then(() => console.log("Bot launched.\n"), nodesStatus.main())
  .catch((err) => console.log(err));
